#!/bin/bash

create_layer () {
	rm -f ./layers/$1.zip
	cd ./layers/$1
	zip -r $1.zip ./*
	mv $1.zip ..
	cd ${OLDPWD}	
}

create_function () {
	cd functions
	first_pwd=${OLDPWD}

	if [ ! -d ./zipped ]; then
		mkdir ./zipped
	fi
	
	rm -f ./zipped/$1.zip
	if [ ! -d "$1" ]; then
		zip -r ./zipped/$1.zip ./$1.*
	else
		cd 	${1}
		zip -r ../zipped/$1.zip ./*
		cd ..
	fi
	cd ${first_pwd}
}

create_web_function () {
	cd website/server/functions
	first_pwd=${OLDPWD}

	if [ ! -d ./zipped ]; then
		mkdir ./zipped
	fi

	rm -f ./zipped/$1.zip
	if [ ! -d "$1" ]; then
		zip -r .zipped/$1.zip ./$1.*
	else
		cd 	${1}
		zip -r ../zipped/$1.zip ./*
		cd ..
	fi
	cd ${first_pwd}
}

# Only need to do once, uncomment when need to install
create_layer "helpers"
create_layer "googleapi"
create_layer "requests"
create_layer "pycurl"
create_layer "pytz"

create_function "ysbma_corection_references"
create_function "ysbma_create_staff"
create_function "ysbma_create_tables"
create_function "ysbma_father_ref"
create_function "ysbma_front_gate"
create_function "ysbma_give_address"
create_function "ysbma_insert_rows"
create_function "ysbma_mother_ref"
create_function "ysbma_prepare_batches"
create_function "ysbma_prepare_correction"

create_web_function "ysbma_api_member"
create_web_function "authorizer"

echo Functions are ready at \`./functions/zipped\` directory and layers at \`./layers\` directory.

