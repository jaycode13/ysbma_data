import pytz
from log_helper import *
import datetime

TZ = pytz.timezone('Asia/Jakarta')

GENDERS = {'male': 'Pria 男', 'female': 'Wanita 女', 'unknown': 'Tidak Tahu 未知'}

TABLE_MEMBERS = "public.members_v2"
TABLE_STAFF = "public.staff"
TABLE_PHONE_NUMBERS = "public.phone_numbers"

# Can view and edit
GROUP_SUPERUSER = 'superuser'

# Can only view
GROUP_USER = 'user'

# Todo: Move to db_helpers if robust enough
# Note: Does not return error. For UPDATE and INSERT use cursor directly.
def execute_query(cursor, query, inputs=[], get_result=True):
    cursor.execute(query, inputs)

    if get_result:
        return cursor.fetchall()
    else:
        return True


def parse_birthdate(date):
    if date:
        return date.strftime('%-d %b %Y')
    else:
        return ''


# Get now datetime in current timezone
def now():
    return datetime.datetime.utcnow().replace(tzinfo=pytz.utc).astimezone(TZ)


def fullname(idx, name_id, name_ch, name_py):
    name1 = [name_id, name_ch, name_py]
    name2 = [i for i in name1 if i]
    return "{} (ID: {})".format(', '.join(name2), idx)
