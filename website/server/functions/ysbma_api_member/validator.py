from globals import *

def has_gender_for_child_ref(member):
    if member['gender'] == GENDERS['male'] or \
       member['gender'] == GENDERS['female']:
        return (True, {})
    else:
        return (False, {
            'errorType': 'NoGenderChildRef',
            'errorMessage': "Orang tua harus memiliki jenis kelamin agar dapat menambahkan anak. Pilih jenis kelamin pada form ini.",
            'highlightSelector': "#gender-male, #gender-female, #gender-unknown",
            'highlightText': "Pilih {} atau {}".format(GENDERS['male'], GENDERS['female']),
            'highlightTextSelector': '#gender-error'
        })

# Checks if a member already has a father id when added as a child.
def has_another_father_id(member, cursor):
    if member['father_id'] != None:
        result = cursor.execute("SELECT id, name_id, name_ch, name_py FROM {} WHERE id=%s".format(TABLE_MEMBERS), member['father_id'])
        return False