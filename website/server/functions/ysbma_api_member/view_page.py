from globals import *
import auth_helpers
from db_helper import *

REDIRECT_URI = "https://ysbmawebsite.s3.amazonaws.com/view.html"
DASHBOARD_URI = "https://ysbmawebsite.s3.amazonaws.com/index.html"

# TODO
def find_similar_names(event, cursor):
    query =("SELECT levenshtein(UPPER(%s), UPPER(name_id)) "
            "levenshtein(%s, name_ch) "
            "levenshtein(UPPER(%s), UPPER(name_id)) AS dif, "
            "name_id, name_ch, name_py "
            "FROM {} ORDER BY dif ASC".format(TABLE_MEMBERS))


def get_collectors(cursor):
    query = ("SELECT id, name_id, name_ch, name_py FROM {} WHERE role='COLLECTOR' ORDER BY name_id".format(TABLE_STAFF))
    cursor.execute(query)
    rows = cursor.fetchall()
    objs = []
    for row in rows:
        objs.append({
            'id': row[0],
            'text': fullname(row[0], row[1], row[2], row[3])
        })
    return objs


def get_inputters(cursor):
    query = ("SELECT id, name_id, name_ch, name_py FROM {} WHERE role='INPUTTER' ORDER BY name_id".format(TABLE_STAFF))
    cursor.execute(query)
    rows = cursor.fetchall()
    objs = []
    for row in rows:
        objs.append({
            'id': row[0],
            'text': fullname(row[0], row[1], row[2], row[3])
        })
    return objs


def view_page(event, cursor):
    params = event["params"]["querystring"]
    member_id = params["member_id"]
    redirect_uri_with_state = "{}&state=id={}".format(REDIRECT_URI, member_id)
    status, rval = auth_helpers.authenticate(params, REDIRECT_URI, redirect_uri_with_state, allowed_groups=[GROUP_USER, GROUP_SUPERUSER])

    if not status:
        return rval
    else:
                      # 0, 1, 2, 3
        selectquery = ("SELECT m.id, m.is_verified, to_char(m.collected_at, 'YYYY-MM-DD HH24:MI:SS') AS collected_at_str, m.collector_id, " 
                      # 4
                      "COALESCE(collector.name_id, '') AS collector_name_id, "
                      # 5, 6
                      "to_char(m.inputted_at, 'YYYY-MM-DD HH24:MI:SS') AS inputted_at_str, m.inputter_id, "
                      # 7
                      "COALESCE(inputter.name_id, '') AS inputter_name_id, "
                      # 8 , 9
                      "to_char(m.updated_at, 'YYYY-MM-DD HH24:MI:SS') AS updated_at_str, m.updated_by, "
                      # 10, 11, 12, 13, 14
                      "m.page_index, m.is_deceased, m.from_xian_you, m.name_id, m.name_py, "
                      # 15, 16, 17, 18, 19
                      "m.name_ch, m.gender, to_char(m.birthdate, 'DD Mon YYYY') AS birthdate_str, m.address, m.city, "
                      # 20, 21, 22, 23, 24
                      "m.email, m.num_siblings, m.num_children, m.num_grandchildren, p.phone_numbers, "
                      # 25, 26, 27, 28
                      "m.father_id, "
                      "COALESCE(mf.name_ch, '') AS father_name_ch, "
                      "COALESCE(mf.name_id, '') AS father_name_id, "
                      "COALESCE(mf.name_py, '') AS father_name_py, "
                      # 29, 30, 31, 32
                      "m.mother_id, "
                      "COALESCE(mm.name_ch, '') AS mother_name_ch, "
                      "COALESCE(mm.name_id, '') AS mother_name_id, "
                      "COALESCE(mm.name_py, '') AS mother_name_py, "
                      # 33, 34, 35, 36
                      "m.spouse_id, "
                      "COALESCE(ms.name_ch, '') AS spouse_name_ch, "
                      "COALESCE(ms.name_id, '') AS spouse_name_id, "
                      "COALESCE(ms.name_py, '') AS spouse_name_py "

                      "FROM {0} AS m "
                      # Collector and Inputter
                      "LEFT JOIN {1} AS collector ON m.collector_id = collector.id AND collector.role = 'COLLECTOR' "
                      "LEFT JOIN {1} AS inputter ON m.inputter_id = inputter.id AND inputter.role = 'INPUTTER' "
                      # Phone numbers
                      "LEFT JOIN ( "
                          "SELECT member_id, COUNT(*) AS num_phones, string_agg(value, ', ') AS phone_numbers "
                          "FROM {2} "
                          "GROUP BY member_id "
                      ") AS p ON m.id = p.member_id "
                      # Father, mother, and spouse
                      "LEFT JOIN {0} mf ON mf.id = m.father_id "
                      "LEFT JOIN {0} mm ON mm.id = m.mother_id "
                      "LEFT JOIN {0} ms ON ms.id = m.spouse_id "
                      "WHERE m.id=%s".format(TABLE_MEMBERS, TABLE_STAFF, TABLE_PHONE_NUMBERS))
        
        result = execute_query(cursor, selectquery, inputs=[member_id], get_result=True)

        if type(result) == list:
            if len(result) == 0:
                return {
                    'token_response': rval,
                    'errorType': 'MemberNotFound',
                    'errorMessage': "Data dengan ID {} tidak ditemukan".format(member_id),
                    'redirectTo': DASHBOARD_URI,
                    'redirectToText': "Kembali ke Dashboard"
                }
            else:
                result = result[0]
        else:
            return {
                'token_response': rval,
                'errorType': 'QueryError',
                'errorMessage': "Terjadi kesalahan pada server.",
                'stackTrace': result['body']
            }

        return {
            'token_response': rval,
            'data': {
                'id' : result[0],
                'is_verified': result[1],
                'collected_at': result[2],
                'collector_id': result[3],
                'collector_name_id': result[4],
                'inputted_at': result[5],
                'inputter_id': result[6],
                'inputter_name_id': result[7],
                'updated_at': result[8],
                'updated_by': result[9],
                'page_index': result[10],
                'is_deceased': result[11],
                'from_xian_you': result[12],
                'name_id' : result[13],
                'name_py' : result[14],
                'name_ch' : result[15],
                'gender'  : result[16],
                'birthdate' : result[17],
                'address'   : result[18],
                'city'    : result[19],
                'email' : result[20],
                'num_siblings': result[21],
                'num_children': result[22],
                'num_grandchildren': result[23],
                'phone_numbers': result[24],
                'father_id': result[25],
                'father_name_ch': result[26],
                'father_name_id': result[27],
                'father_name_py': result[28],
                'mother_id': result[29],
                'mother_name_ch': result[30],
                'mother_name_id': result[31],
                'mother_name_py': result[32],
                'spouse_id': result[33],
                'spouse_name_ch': result[34],
                'spouse_name_id': result[35],
                'spouse_name_py': result[36],
            },
            'collectors': get_collectors(cursor),
            'inputters': get_inputters(cursor)
        }


def new_member_page(event, cursor):
    params = event['params']['querystring']

    status, rval = auth_helpers.authenticate(params, REDIRECT_URI, allowed_groups=[GROUP_SUPERUSER])

    if not status:
        return rval

    return {
        'token_response': rval,
        'collectors': get_collectors(cursor),
        'inputters': get_inputters(cursor)
    }


def get_offsprings(event, cursor):
    params = event['params']['querystring']

    status, rval = auth_helpers.authenticate(params, REDIRECT_URI)

    if not status:
        return rval

    member_id = params['member_id']
    member = {'id': member_id, 'text': '', 'gender': '', 'children': []}

    levels = 3
    family_tree = get_family_tree(member, 1, levels, cursor)
    return family_tree['children']


# For testing the recursive function.
def get_family_tree_test(member, level, max_level, cursor):
    children = [{'level': level, 'children': []}]
    for child in children:
        if level < max_level:
            child = get_family_tree_test(child, level+1, max_level, cursor)
        member['children'].append(child)
    return member


def get_family_tree(member, level, max_level, cursor):
    # Get offsprings from db
    # Add to member's children
    # level + 1
    # run get_family_tree for each children until level reaches max_level.
    children = get_offsprings_from_db(member, cursor)
    for child in children:
        if level < max_level:
            child = get_family_tree(child, level+1, max_level, cursor)
        member['children'].append(child)
    return member


def get_offsprings_from_db(parent, cursor):
    # Get the offsprings of that parent.
    offsprings = []
    # 0: id, 1: name_id, 2: name_ch, 3: name_py, 4: gender, 5: age, 6: birthdate
    query = ("SELECT id, name_id, name_ch, name_py, "
             "gender, extract(year from age(CURRENT_DATE, birthdate)) AS age, birthdate, "
             # 7: address, 8: city
             "address, city "
             "FROM {0} WHERE father_id = %s OR mother_id = %s ORDER BY birthdate DESC, id ASC").format(TABLE_MEMBERS)

    cursor.execute(query, [parent['id'], parent['id']])
    result = cursor.fetchall()

    for offspring in result:
        # If more than 5 columns, we know that the result is okay. Otherwise
        # there might be something wrong with the query or algorithm.
        if len(offspring) > 5:
            name1 = [offspring[1], offspring[2], offspring[3]]
            name2 = [i for i in name1 if i]
            label = "{} (ID: {})".format(', '.join(name2), offspring[0])
            obj = {'id': offspring[0],
                   'text': label,
                   'gender': offspring[4],
                   'age': offspring[5],
                   'birthdate': parse_birthdate(offspring[6]),
                   'address': offspring[7],
                   'city': offspring[8],
                   'children': []}
        else:
            obj = {'id': None, 'children': [], 'errorMessage': offspring}
        offsprings.append(obj)

    return offsprings
