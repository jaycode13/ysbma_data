import requests, base64
import boto3
from credentials import *

def get_auth_header():
  return 'Basic {}'.format(base64.b64encode('{}:{}'.format(CLIENT_ID, CLIENT_SECRET).encode(encoding='UTF-8')).decode('UTF-8'))


# For redirect_uri_with_state, when need to include a dynamic state,
# add "&state=param1=value1+param2=value2".
# References:
# - https://stackoverflow.com/questions/51143646/querystring-parameters-in-callback-url-for-aws-cognito
# - https://github.com/aws-amplify/amplify-js/issues/755
#
# Remember that redirect_uri in the data parameter must be set with the redirect_uri
# without state parameter.

# AuthError will trigger removal of user session.
def authenticate(params, redirect_uri, redirect_uri_with_state=None, allowed_groups=None):
    if redirect_uri_with_state is None:
        redirect_uri_with_state = redirect_uri

    login_url = LOGIN_URL.format(
        CLIENT_ID,
        redirect_uri_with_state
    )

    if 'refresh_token' not in params and 'code' not in params:
        return (False, {
            'errorMessage': "Silakan login kembali.",
            'errorType': "TokenNotFound",
            'redirectTo': login_url,
            'redirectToText': "Kembali ke halaman login"
        })

    # PREPARE DATA
    # Try to use refresh_token first.
    if 'refresh_token' in params:
        refresh_token = params['refresh_token']
        data = {
            'grant_type': 'refresh_token',
            'client_id': CLIENT_ID,
            'refresh_token': refresh_token
        }
    # If not found, use code
    elif 'code' in params:
        code = params['code']
        data = {
            'grant_type': 'authorization_code',
            'client_id': CLIENT_ID,
            'code': code,
            'redirect_uri': redirect_uri
        }

    # PREPARE HEADER
    auth = get_auth_header()
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': auth
    }

    try:
        r = requests.post(OAUTH_TOKEN_URL, data=data, headers=headers)
    except:
        return (False, {
            'errorMessage': "Tidak dapat otentikasi data.",
            'errorType': "AuthError",
            'redirectTo': login_url,
            'redirectToText': "Kembali ke halaman login"
        })

    # When correct, rval should contain `access_token`, `refresh_token`, and `bearer`.
    rval = r.json()
    if 'error' in rval:
        if rval['error'] == 'invalid_grant':
            # Token has been revoked. Login again.
            return (False, {
                'errorMessage': "Token sudah kadaluarsa. Silakan login kembali.",
                'errorType': "ExpiredToken",
                'redirectTo': login_url,
                'redirectToText': "Kembali ke halaman login"
            })
        else:
            # Token has been revoked. Login again.
            return (False, {
                'errorMessage': "Authentication Error: {}".format(rval['error']),
                'errorType': "AuthError",
                'redirectTo': login_url,
                'redirectToText': "Kembali ke halaman login"
            })
    else:
        # Get user info.
        # -------------------------------------------------
        auth = 'Bearer {}'.format(rval['access_token'])

        r = requests.get(OAUTH_INFO_URL, headers={'Authorization': auth})
        username = r.json()['username']
        rval['username'] = username

        client = boto3.client('cognito-idp')
        lg_response = client.admin_list_groups_for_user(Username=username,UserPoolId=POOL_ID)
        groups = lg_response['Groups']
        groupname = ""
        if len(groups) > 0:
            groupname = groups[0]['GroupName']
            # # For when we need to present the groups. Otherwise we'd get Marshal error from date objects.
            # for group in groups:
            #     group['LastModifiedDate'] = group['LastModifiedDate'].strftime("%Y-%m-%d %H:%M:%S")
            #     group['CreationDate'] = group['CreationDate'].strftime("%Y-%m-%d %H:%M:%S")

        rval['group'] = groupname
        # -------------------------------------------------

        # Group authorization
        # -------------------------------------------------
        if allowed_groups is not None:
            if groupname not in allowed_groups:
                return False, {
                    'errorMessage': "Fitur ini hanya dapat diakses oleh: {}".format(', '.join(allowed_groups)),
                    # AuthError will trigger removal of user session, so we use GroupAuthError.
                    'errorType': "GroupAuthError",
                    'redirectTo': login_url,
                    'redirectToText': "Kembali ke halaman login"
                }
        # -------------------------------------------------
        return (True, rval)