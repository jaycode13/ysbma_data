from globals import *
import auth_helpers
from db_helper import *
import datetime
import validator
import json
import psycopg2
import os


REDIRECT_URI = "https://ysbmawebsite.s3.amazonaws.com/view.html"

def none_if_empty(value):
    return None if value == '' else value

def format_numeric(value):
    return None if value == '' else int(value)


def prepare_inputs(data):
    data['inputted_at'] = none_if_empty(data['inputted_at'])
    data['collected_at'] = none_if_empty(data['collected_at'])
    data['birthdate'] = none_if_empty(data['birthdate'])

    data['collector_id'] = format_numeric(data['collector_id'])
    data['inputter_id'] = format_numeric(data['inputter_id'])
    data['num_siblings'] = format_numeric(data['num_siblings'])
    data['num_children'] = format_numeric(data['num_children'])
    data['num_grandchildren'] = format_numeric(data['num_grandchildren'])
    data['father_id'] = format_numeric(data['father_id'])
    data['mother_id'] = format_numeric(data['mother_id'])
    data['spouse_id'] = format_numeric(data['spouse_id'])


def edit_member(event, cnx):
    params = event['params']['querystring']
    data = json.loads(params["data"])
    member_id = data["id"]

    redirect_uri_with_state = "{}&state=id={}".format(REDIRECT_URI, member_id)
    status, rval = auth_helpers.authenticate(params, REDIRECT_URI, redirect_uri_with_state, allowed_groups=[GROUP_SUPERUSER])

    if not status:
        return rval

    # uniq_member, dup = validator.member_is_unique(data)
    # if !uniq_member:
    #     return {
    #         'errorType': 'ValidationError',
    #         'errorMessage': 'Ada member lain dengan nama sama: <a href="{}" target="_blank">{} (ID: {})</a>'.format(
    #             "{}?id={}".format(REDIRECT_URI, dup['id']),
    #             [dup['name_id'], dup['name_ch'], dup['name_py']].join(', '),
    #             dup['id']
    #         )
    #     }

    cnx.autocommit = False
    with cnx, cnx.cursor() as cursor:

        query = ("UPDATE {0} SET "
                 # 0, 1, 2
                 "is_verified=%s, collected_at=%s, collector_id=%s, "
                 # 3, 4, 5
                 "inputted_at=%s, inputter_id=%s, updated_at=TIMESTAMP %s, "
                 # 6, 7, 8
                 "page_index=%s, is_deceased=%s, from_xian_you=%s, "
                 # 9, 10, 11
                 "name_id=%s, name_ch=%s, name_py=%s, "
                 # 12, 13, 14
                 "gender=%s, birthdate=%s, address=%s, "
                 # 15, 16, 17
                 "city=%s, email=%s, num_siblings=%s, "
                 # 18, 19
                 "num_children=%s, num_grandchildren=%s, "
                 # 20, 21, 22
                 "father_id=%s, mother_id=%s, spouse_id=%s, "
                 # 23
                 "updated_by=%s"
                 # id
                 "WHERE id=%s".format(TABLE_MEMBERS))

        prepare_inputs(data)
        cursor.execute(query, [
            # 0, 1, 2
            data['is_verified'], data['collected_at'], data['collector_id'],
            # 3, 4, 5
            data['inputted_at'], data['inputter_id'], now(),
            # 6, 7, 8
            data['page_index'], data['is_deceased'], data['from_xian_you'],
            # 9, 10, 11
            data['name_id'], data['name_ch'], data['name_py'],
            # 12, 13, 14
            data['gender'], data['birthdate'], data['address'],
            # 15, 16, 17
            data['city'], data['email'], data['num_siblings'],
            # 18, 19
            data['num_children'], data['num_grandchildren'],
            # 20, 21, 22
            data['father_id'], data['mother_id'], data['spouse_id'],
            # 23
            rval['username'],
            # id
            data['id']
        ])

        # Phone numbers
        if 'phone_numbers' in data:
            phone_numbers = [x.strip() for x in data['phone_numbers'].split(',')]
            phone_numbers = tuple(set(phone_numbers))
            # Find phone numbers in database that are not in the list of phone numbers to delete.
            query = "DELETE FROM {} WHERE value NOT IN %s AND member_id=%s".format(TABLE_PHONE_NUMBERS)

            cursor.execute(query, [phone_numbers, data['id']])

            qplaceholders = []
            qvalues = []
            for num in phone_numbers:
                qplaceholders.append("(%s, %s, TIMESTAMP %s, TIMESTAMP %s)")
                qvalues.append(data['id'])
                qvalues.append(str(num))
                qvalues.append(now())
                qvalues.append(now())

            # Find phone numbers in the list that are not in the database to add.
            query = ("INSERT INTO {}("
                     # 0, 1, 2, 3
                     "member_id, value, created_at, updated_at) "
                     "values {} ON CONFLICT (member_id, value) DO NOTHING".format(
                        TABLE_PHONE_NUMBERS,
                        ', '.join(qplaceholders)
                    ))

            cursor.execute(query, qvalues)

    return {
        'token_response': rval,
        'status': 'OK'
    }


def search_member(event, cursor):
    params = event['params']['querystring']

    status, rval = auth_helpers.authenticate(params, REDIRECT_URI, allowed_groups=[GROUP_SUPERUSER, GROUP_USER])

    if not status:
        return rval

    query = ("SELECT id, name_id, name_ch, name_py FROM {0} "
             "WHERE id=%s OR UPPER(name_id) LIKE UPPER(%s) OR "
             "name_ch LIKE %s OR UPPER(name_py) LIKE UPPER(%s) "
             "LIMIT 100".format(TABLE_MEMBERS))
    like_var = '%{}%'.format(params['term'])
    idx = 0
    if params['term'].isnumeric():
        idx = params['term']

    # like_var times the number of %s.
    like_vars = [idx] + [like_var] * 3
    cursor.execute(query, like_vars)
    rows = cursor.fetchall()

    result = []
    for row in rows:
        label = fullname(row[0], row[1], row[2], row[3])

        obj = {'id': row[0], 'value': label, 'label': label}
        result.append(obj)
    return result


def add_child(event, cnx):
    params = event['params']['querystring']
    status, rval = auth_helpers.authenticate(params, REDIRECT_URI, allowed_groups=[GROUP_SUPERUSER])
    if not status:
        return rval

    cnx.autocommit = True
    cursor = cnx.cursor()

    query = "SELECT id, gender FROM {} WHERE id=%s".format(TABLE_MEMBERS)
    cursor.execute(query, [params['member_id']])
    rows = cursor.fetchall()
    if len(rows) == 0:
        return {
            'errorType': 'MemberNotFound',
            'errorMessage': "Member dengan ID {} tidak ditemukan".format(params['member_id'])
        }
    row = rows[0]

    member = {
        'id': row[0],
        'gender': row[1]
    }

    # If gender is unknown, fails it.
    isvalid, err = validator.has_gender_for_child_ref(member)
    if not isvalid:
        return err

    # TODO: If child has another parent, send a warning.

    cnx.autocommit = False
    with cnx, cnx.cursor() as cursor:

        if member['gender'] == GENDERS['male']:
            query = ("UPDATE {} SET father_id=%s, updated_at=TIMESTAMP %s, updated_by=%s WHERE id=%s").format(TABLE_MEMBERS)
        elif member['gender'] == GENDERS['female']:
            query = ("UPDATE {} SET mother_id=%s, updated_at=TIMESTAMP %s, updated_by=%s WHERE id=%s").format(TABLE_MEMBERS)

        cursor.execute(query, (member['id'], now(), rval['username'], params['newchild_id']))

        # Logging
        query = "UPDATE {} SET updated_at=TIMESTAMP %s, updated_by=%s WHERE id=%s".format(TABLE_MEMBERS)
        cursor.execute(query, (now(), rval['username'], params['member_id']))

    return {
        'status': 'OK'
    }


# Pass in member_id and child_id
def delete_child(event, cnx):
    params = event['params']['querystring']
    status, rval = auth_helpers.authenticate(params, REDIRECT_URI, allowed_groups=[GROUP_SUPERUSER])
    if not status:
        return rval

    cnx.autocommit = False
    with cnx, cnx.cursor() as cursor:

        # Get member's gender
        query = "SELECT id, gender FROM {} WHERE id=%s".format(TABLE_MEMBERS)
        cursor.execute(query, [params['member_id']])
        member = cursor.fetchone()
        field = 'father_id'
        if member[1] == GENDERS['female']:
            field = 'mother_id'

        # Get child
        query = "UPDATE {} SET {}=%s, updated_at=TIMESTAMP %s, updated_by=%s WHERE id=%s".format(TABLE_MEMBERS, field)
        cursor.execute(query, (None, now(), rval['username'], params['child_id']))

        # Logging
        query = "UPDATE {} SET updated_at=TIMESTAMP %s, updated_by=%s WHERE id=%s".format(TABLE_MEMBERS)
        cursor.execute(query, (now(), rval['username'], params['member_id']))

    return {
        'status': 'OK'
    }


def delete_member(event, cnx):
    params = event['params']['querystring']
    status, rval = auth_helpers.authenticate(params, REDIRECT_URI, allowed_groups=[GROUP_SUPERUSER])
    if not status:
        return rval

    cnx.autocommit = False
    with cnx, cnx.cursor() as cursor:

        # Remove phone numbers
        query = "DELETE FROM {0} WHERE member_id=%s".format(TABLE_PHONE_NUMBERS)
        cursor.execute(query, [params['member_id']])

        # Update offsprings (i.e. remove father_id and mother_id that reference this object)
        query = ("UPDATE {} SET father_id=NULL, updated_at=TIMESTAMP %s, "
                 "updated_by=%s WHERE father_id=%s "
                 "RETURNING id, name_id, name_ch, name_py".format(TABLE_MEMBERS))
        cursor.execute(query, (now(), rval['username'], params['member_id']))
        # rows = fetchall()
        # if len(rows) != 0:
        #     row = rows[0]
        #     father_name = fullname(row[0], row[1], row[2], row[3])

        query = ("UPDATE {} SET mother_id=NULL, updated_at=TIMESTAMP %s, updated_by=%s WHERE mother_id=%s".format(TABLE_MEMBERS))
        cursor.execute(query, (now(), rval['username'], params['member_id']))

        # Update spouse (i.e. remove spouse_id)
        query = ("UPDATE {} SET spouse_id=NULL, updated_at=TIMESTAMP %s, updated_by=%s WHERE spouse_id=%s".format(TABLE_MEMBERS))
        cursor.execute(query, (now(), rval['username'], params['member_id']))
        
        query = 'DELETE FROM {} WHERE id=%s'.format(TABLE_MEMBERS)
        result = cursor.execute(query, [params['member_id']])
    return {
        'token_response': rval,
        'status' : 'OK',
        # 'father_name': father_name
    }


def new_member(event, cnx):
    params = event['params']['querystring']
    status, rval = auth_helpers.authenticate(params, REDIRECT_URI, allowed_groups=[GROUP_SUPERUSER])
    if not status:
        return rval

    data = json.loads(params['data'])

    cnx.autocommit = False
    with cnx, cnx.cursor() as cursor:
        query = ("INSERT INTO {}("
                     # 0, 1, 2
                     "is_verified, collected_at, collector_id, "
                     # 3, 4, 5
                     "inputted_at, inputter_id, updated_at, "
                     # 6, 7, 8
                     "page_index, is_deceased, from_xian_you, "
                     # 9, 10, 11
                     "name_id, name_ch, name_py, "
                     # 12, 13, 14
                     "gender, birthdate, address, "
                     # 15, 16, 17
                     "city, email, num_siblings, "
                     # 18, 19
                     "num_children, num_grandchildren, "
                     # 20, 21, 22
                     "father_id, mother_id, spouse_id, "
                     # 23, #24
                     "updated_by, created_at "
                 ") values("
                     "%s, %s, %s, %s, %s, TIMESTAMP %s, "
                     "%s, %s, %s, %s, %s, %s, "
                     "%s, %s, %s, %s, %s, %s, "
                     "%s, %s, %s, %s, %s, %s, TIMESTAMP %s "
                 ") RETURNING id".format(TABLE_MEMBERS))

        prepare_inputs(data)
        cursor.execute(query, [
            # 0, 1, 2
            data['is_verified'], data['collected_at'], data['collector_id'],
            # 3, 4, 5
            data['inputted_at'], data['inputter_id'], now(),
            # 6, 7, 8
            data['page_index'], data['is_deceased'], data['from_xian_you'],
            # 9, 10, 11
            data['name_id'], data['name_ch'], data['name_py'],
            # 12, 13, 14
            data['gender'], data['birthdate'], data['address'],
            # 15, 16, 17
            data['city'], data['email'], data['num_siblings'],
            # 18, 19
            data['num_children'], data['num_grandchildren'],
            # 20, 21, 22
            data['father_id'], data['mother_id'], data['spouse_id'],
            # 23, 24
            rval['username'], now()
        ])

        idx = cursor.fetchone()

        # Phone numbers
        if 'phone_numbers' in data:
            phone_numbers = [x.strip() for x in data['phone_numbers'].split(',')]
            phone_numbers = tuple(set(phone_numbers))

            qplaceholders = []
            qvalues = []
            for num in phone_numbers:
                qplaceholders.append("(%s, %s, TIMESTAMP %s, TIMESTAMP %s)")
                qvalues.append(idx)
                qvalues.append(str(num))
                qvalues.append(now())
                qvalues.append(now())

            # Find phone numbers in the list that are not in the database to add.
            query = ("INSERT INTO {}("
                     # 0, 1, 2, 3
                     "member_id, value, created_at, updated_at) "
                     "values {} ON CONFLICT (member_id, value) DO NOTHING".format(
                        TABLE_PHONE_NUMBERS,
                        ', '.join(qplaceholders)
                    ))

            cursor.execute(query, qvalues)

    return {
        "status" : "OK",
        "data": {
            "id" : idx,
        }
    }