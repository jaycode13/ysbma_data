"""
Layers:
1. helpers
2. psycopg2
3. requests
4. pytz
"""
import json
import os
from db_helper import *
import quicksight_related as qr
import view_page as view
import editor

def lambda_handler(event, context):
    try:
        cnx = make_connection(os.environ.get('ENDPOINT'),
                              os.environ.get('PORT'),
                              os.environ.get('DBUSER'),
                              os.environ.get('DBPASSWORD'),
                              os.environ.get('DATABASE'))
    except:
        return "ERROR: Cannot connect to database from handler.\n{}".format(traceback.format_exc())
        
    if event["context"]["http-method"]=="GET":
        cursor=cnx.cursor()
        return FUNC_GET(event,cursor)
        
    elif event["context"]["http-method"]=="PUT":
        return FUNC_PUT(event, cnx)
    
    elif event["context"]["http-method"]=="POST":
        cursor=cnx.cursor()
        return include_headers(FUNC_POST(event, cursor))
        # return FUNC_POST(event, cursor)
        
    elif event["context"]["http-method"]=="DELETE":
        # Delete is a little dangerous, so we use transaction.
        # This is important to avoid missing references when
        # deletion is unsuccessful
        return FUNC_DELETE(event, cnx)
        
    else:
        return include_headers(event)

def include_headers(body):
    response = {
        'headers': {
            'Content-Type': 'application/json', 
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Content-Type,Access-Control-Allow-Origin'
        },
        'body': body,
        'statusCode': 200
    }
    return response

def FUNC_GET(event, cursor):
    params = event["params"]["querystring"]

    if 'action' in params and params['action'] == 'new_member':
        return view.new_member_page(event, cursor)

    if 'member_id' not in params:
        if 'action' in params and params['action'] == 'search':
            return editor.search_member(event, cursor)
        else:
            return qr.main_page(event)
    else:
        if 'action' in params and params['action'] == 'get_offsprings':
            return view.get_offsprings(event, cursor)
        else:
            return view.view_page(event, cursor)
    
def FUNC_PUT(event, cnx):
    params = event["params"]["querystring"]
    if 'action' in params and params['action'] == 'add_child':
        return editor.add_child(event, cnx)
    elif 'action' in params and params['action'] == 'add_member':
        return editor.new_member(event, cnx)
    else:
        return editor.edit_member(event, cnx)

# TODO: Fix CORS error for POST
def FUNC_POST(event, cursor):
    # Broken. CORS error.
    return {'a': 1}
    return editor.new_member(event, cursor)


def FUNC_DELETE(event, cnx):
    params = event["params"]["querystring"]
    if 'action' in params and params['action'] == 'delete_child':
        return editor.delete_child(event, cnx)
    else:
        return editor.delete_member(event, cnx)
    
def getTotalPage(cursor):
    countQuery = 'SELECT COUNT(*) FROM public.members_v2'
    result = execute_query(cursor, query = countQuery, get_result=True)[0][0]
    return result

def getMember(cursor, limit=10, offset=0):
    baseSql = "SELECT * FROM public.members_v2 LIMIT %s OFFSET %s"
    
    return_member = []
    members = execute_query(cursor, baseSql, inputs = [limit,offset], get_result=True)
    for member in members:
        return_member.append(
            {
                'id' : member[0],
                'name_id' : member[2],
                'name_ch' : member[3],
                'name_py' : member[4]
            }
        )
    
    return return_member

