import requests, base64
import boto3
import urllib.parse
import auth_helpers
from credentials import *

LOGIN_URL = LOGIN_URL.format(CLIENT_ID, REDIRECT_URI)


def main_page(event):
    params = event["params"]["querystring"]
    if 'action' in params and params['action'] == 'validate':
        # Get tokens by POST-ing code parameter to OAuth server

        status, rval = auth_helpers.authenticate(params, REDIRECT_URI)

        if not status:
            return rval
        else:

            # Assume role that has quicksight dashboard access.
            # -------------------------------------------------
            sts_default_provider_chain = boto3.client('sts')

            identity_str = sts_default_provider_chain.get_caller_identity()['Arn']

            role_to_assume_arn=ROLE_ARN
            role_session_name='quicksight_session'

            response=sts_default_provider_chain.assume_role(
                RoleArn=role_to_assume_arn,
                RoleSessionName=role_session_name
            )

            creds=response['Credentials']
            # -------------------------------------------------


            # Use the assumed role to login to get dashboard url.
            # -------------------------------------------------

            import botocore.session
            default_namespace = 'default'
            account_id = ACCOUNT_ID

            session = botocore.session.get_session()
            client = boto3.client('quicksight',
                region_name='us-east-1',
                aws_access_key_id=creds['AccessKeyId'],
                aws_secret_access_key=creds['SecretAccessKey'],
                aws_session_token=creds['SessionToken'],
            )

            # response = client.list_users(AwsAccountId = account_id, Namespace=default_namespace)

            url = client.get_dashboard_embed_url(
                AwsAccountId=ACCOUNT_ID,
                DashboardId=DASHBOARD_ID,
                # IdentityType="IAM",
                IdentityType="QUICKSIGHT",
                SessionLifetimeInMinutes=100,
                ResetDisabled=False,
                UndoRedoDisabled=False,
                UserArn=QUICKSIGHT_USER_ARN
            )
            # -------------------------------------------------

            return {
                'url': url['EmbedUrl'],
                'token_response': rval,
            }
    else:
        # Otherwise, tells client to login.
        return {
            'errorMessage': "Token sudah kadaluarsa. Silakan login kembali.",
            'errorType': "ExpiredToken",
            'redirectTo': LOGIN_URL,
            'redirectToText': "Kembali ke halaman login"
        }
