'use strict';

var LOGIN_URL = "https://ysbma-data.auth.us-east-1.amazoncognito.com/login?response_type=code&client_id=41518u0rvtte1lmre6l35vgnd0&redirect_uri=";

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        var value = sParameterName.slice(1, sParameterName.length).join('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(value);
        }
    }
}


function formatParams(params) {
    return "?" + Object
        .keys(params)
        .map(function(key){
          return key+"="+encodeURIComponent(params[key])
        })
        .join("&")
}


function showLoading() {
    $('#loading').show();
}


function hideLoading() {
    $('#loading').hide();
}


/* Set alert message in cookies to be displayed in the next page refresh. */
function setAlertMessage(message) {
    Cookies.set('alert_message', message, { sameSite: 'None', secure: true });
}


function displayAlertMessage() {
    $('#alert-container').show();
}


function initializeAlertMessage() {
    if (Cookies.get('alert_message') != undefined) {
        $('#alert-content').text(Cookies.get('alert_message'));
        displayAlertMessage();
    }
    $('#alert-container').on('closed.bs.alert', function() {
        Cookies.remove('alert_message');
    });
}


/* options: {errorType: text, redirectTo: text, redirectToText: text, stackTrace: text,
             highlightSelector: text, // Selector to add class '.is-invalid'
             highlightText: text // Feedback to write
             highlightTextSelector: text // Insert feedback next to this element.
            }*/
function showError(title, message, options) {
    $('#msg-error').modal();
    $('#msg-error-title').text(title);
    $('#msg-error-content').text(message);
    $('#msg-error-btn').attr('href', '#');
    if (options != undefined) {
        if (options.errorType != undefined) {
            $('#msg-error-content').append($("<hr />"));
            $('#msg-error-content').append("Error Type: " + options.errorType);
        }
        if (options.redirectTo != undefined) {
            $('#msg-error-btn').attr('href', options.redirectTo);
            $('#msg-error-btn').on('click', function() {window.location.href=options.redirectTo})
        }
        if (options.redirectToText != undefined) {
            $('#msg-error-btn').text(options.redirectToText);
        }
        if (options.stackTrace != undefined) {
            console.log(options.stackTrace);
        }
        if (options.highlightSelector != undefined) {
            $(options.highlightSelector).addClass('is-invalid');
        }
        if (options.highlightText != undefined) {
            var selector = options.highlightSelector;
            if (options.highlightTextSelector != undefined) {
                selector = options.highlightTextSelector;
            }
            $(selector).after('<div class="invalid-feedback">'+options.highlightText+'</div>');
        }
    }
}


// If no refresh_token or code parameter, go to login page.
function verifyLoginStatus(redirect_uri) {
    if (Cookies.get('refresh_token') == undefined &&
        getUrlParameter('code') == undefined) {
        logout(redirect_uri);
    }
}


function processAssortedError(response) {
    if (response.errorType == 'ExpiredToken' || response.errorType == 'AuthError') {
        // The refresh token has been revoked (invalid_grant error).
        Cookies.remove('refresh_token');
    }

    showError("Oops.", response.errorMessage, {
        errorType: response.errorType,
        redirectTo: response.redirectTo,
        redirectToText: response.redirectToText,
        stackTrace: response.stackTrace,
        highlightSelector: response.highlightSelector,
        highlightText: response.highlightText,
        highlightTextSelector: response.highlightTextSelector
    });
}


function logout(redirect_uri) {
    Cookies.remove('refresh_token');
    showLoading();
    window.location.href = LOGIN_URL + redirect_uri;
}


// function authenticate() {     
//     var url = "https://nvayt9souk.execute-api.us-east-1.amazonaws.com/ysbma_test/member";

//     var params = {
//         'action': 'validate'
//     };

//     var code = getUrlParameter('code');
//     var refresh_token = Cookies.get('refresh_token');
//     if (code != undefined) {
//         params['code'] = code;
//     }
//     if (refresh_token != undefined) {
//         params['refresh_token'] = refresh_token;
//     }

//     return new Promise(function(resolve, reject) {
//         var req = new XMLHttpRequest();
//         req.open('GET', url + formatParams(params));

//         req.onload = function() {
//             var response = JSON.parse(req.responseText);
//             if (req.status == 200) {
//                 if (response != undefined && response.errorType != undefined) {
//                     reject(response);
//                 }
//                 else {
//                     resolve(response);
//                 }
//             }
//             else {
//                 reject(response);
//             }
//         }

//         req.onerror = function() {
//             reject(Error("Network Error"));
//         }

//         req.send();

//     });
// }



function request(HTTPMethod, url, params) {
    var code = getUrlParameter('code');
    var refresh_token = Cookies.get('refresh_token');
    if (code != undefined) {
        params['code'] = code;
    }
    if (refresh_token != undefined) {
        params['refresh_token'] = refresh_token;
    }
    
    return new Promise(function(resolve, reject) {
        var req = new XMLHttpRequest();
        if (HTTPMethod != 'POST') {
            url = url + formatParams(params)
        }
        req.open(HTTPMethod, url, true);

        req.onload = function() {
            var response = JSON.parse(req.responseText);
            if (req.status == 200) {
                if (response != undefined && response.errorType != undefined) {
                    reject(response);
                }
                else {
                    resolve(response);
                }
            }
            else {
                reject(response);
            }
        }

        req.onerror = function() {
            reject({'errorType': "NetworkError", 'errorMessage': "Ada masalah saat menghubungi server. Silakan coba lagi, dan hubungi IT support apabila masalah tetap ada." });
        }

        // req.setRequestHeader("Content-Type", 'application/json');
        req.setRequestHeader('Accept', 'application/json');
        if (HTTPMethod == 'POST') {
            // req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            req.send(JSON.stringify(params));
            // req.send(formatParams(params).slice(1))
        }
        else {
            // req.setRequestHeader("Content-Type", 'application/json');
            req.send();
        }


    });
}


function fullName(id, name_id, name_ch, name_py) {
    var array = [name_id, name_ch, name_py]
    var nonempty = array.filter(function (el) {
      return el != null;
    });
    return nonempty.join(', ') + " (ID: " + id + ")"
}


function calculateAge(birthday) { // birthday is a date
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}


function isValidDate(d) {
  return d instanceof Date && !isNaN(d);
}
