"""
Layers:
1. psycopg2
2. helpers
"""
import json
import os
import traceback
import io
from db_helper import *
    
def lambda_handler(event, context):
    try:
        cnx = make_connection(event['DBConnection']['endpoint'],
                              event['DBConnection']['port'],
                              event['DBConnection']['dbuser'],
                              event['DBConnection']['dbpassword'],
                              event['DBConnection']['database'])
        cursor=cnx.cursor()
        logger.info('Connected!')
    except:
        return log_err("ERROR: Cannot connect to database from handler.\n{}".format(traceback.format_exc()))
    
    count_member_sql = "select count(id) from {0}.members_v2".format(event["env"])
    cursor.execute(count_member_sql)
    data_count_member = cursor.fetchone()[0]
    
    index = 0
    increment = 500
    batch = []
    while index < data_count_member:
        result = {
            "from" : index,
            "to"   : data_count_member
        }
        if data_count_member > index + increment:
            result = {
                "from" : index,
                "to"   : index + increment
            }
        batch.append(json.dumps(result))
        index = index + increment
        
    batch.append('DONE')
    
    return batch
