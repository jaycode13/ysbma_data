"""
Layers:
1. psycopg2
2. helpers
"""
import json
import os
import traceback
import io
from db_helper import *

def lambda_handler(event, context):
    try:
        cnx = make_connection(event['DBConnection']['endpoint'],
                              event['DBConnection']['port'],
                              event['DBConnection']['dbuser'],
                              event['DBConnection']['dbpassword'],
                              event['DBConnection']['database'])
        cursor=cnx.cursor()
        logger.info('Connected!')
    except:
        return log_err("ERROR: Cannot connect to database from handler.\n{}".format(traceback.format_exc()))
        
    batcher = json.loads(event["Batches"][0])
    bfrom = batcher["from"]
    bto   = batcher["to"]
    
    batch_loader_query = "select * from {0}.member_update_log where status=0".format(event["env"])
    cursor.execute(batch_loader_query)
    batch_loader = cursor.fetchall()
    
    for row in batch_loader:
        queries_update = row[2]
        string = "update {0}.member_update_log set status=1 where id={1}".format(event["env"],row[0])
        execute_query(cursor, string, get_result=False)
        execute_query(cursor, queries_update, get_result=False)
    
    return True
