"""
Layers:
1. psycopg2
2. helpers
"""
import json
import os
import traceback
import io
from db_helper import *

def lambda_handler(event, context):
    try:
        cnx = make_connection(event['DBConnection']['endpoint'],
                              event['DBConnection']['port'],
                              event['DBConnection']['dbuser'],
                              event['DBConnection']['dbpassword'],
                              event['DBConnection']['database'])
        cursor=cnx.cursor()
        logger.info('Connected!')
    except:
        return log_err("ERROR: Cannot connect to database from handler.\n{}".format(traceback.format_exc()))
        
    batcher = json.loads(event["Batches"][0])
    bfrom = batcher["from"]
    bto   = batcher["to"]
    
    mother_ref(
        schema = event["env"],
        cursor = cursor,
        member_limit_from = bfrom,
        member_limit_to = bto
    )
    
    spouse_ref(
        schema = event["env"],
        cursor = cursor,
        member_limit_from = bfrom,
        member_limit_to = bto
    )
    return True
    
def mother_ref(schema,cursor,member_limit_from,member_limit_to):
    motherQuery = "SELECT id, name_id, name_ch, name_py FROM {0}.members_v2 WHERE id >= {1} AND id < {2}".format(schema,member_limit_from,member_limit_to)
    mothers = execute_query(cursor, motherQuery, get_result=True)
    
    getMotherQuery = """
        SELECT m.id, m.name_id, m.name_ch, m.name_py, m.mother_id_name, m.mother_ch_name 
        FROM {0}.members_v1 m
        WHERE 
        m.mother_id_name in (SELECT name_id from public.members_v2 f where f.id>={1} and f.id<={2})
        or
        m.mother_ch_name in (SELECT name_ch from public.members_v2 f where f.id>={1} and f.id<={2})
        or
        m.mother_ch_name in (SELECT name_py from public.members_v2 f where f.id>={1} and f.id<={2})""".format(schema,member_limit_from,member_limit_to)
    members = execute_query(cursor, getMotherQuery, get_result=True)
    
    for member in members:
        for mother in mothers:
            
            if member[4]!=None and member[4]==mother[1]:
                sql = ""
                description = "{id(member_mother|mother)=" + str(member[4]) + "|" + str(mother[1]) + "}"
                
                if member[1]!=None:
                    sql = ("UPDATE {0}.members_v2 SET mother_id ="+str(mother[0]) +" WHERE name_id= '"+str(member[1])+"'").format(schema)
                elif member[2]!=None:
                    sql = ("UPDATE {0}.members_v2 SET mother_id ="+str(mother[0]) +" WHERE name_ch= '"+str(member[2])+"'").format(schema)
                elif member[3]!=None:
                    sql = ("UPDATE {0}.members_v2 SET mother_id ="+str(mother[0]) +" WHERE name_py= '"+str(member[3])+"'").format(schema)
                    
                input_log(cursor=cursor, schema = schema,ref = "MOTHER",logQuery = sql, desc = description)
                break
            
            elif member[5]!=None and member[5]==mother[2]:
                sql = ""
                description = "{ch(member_mother|mother)=" + str(member[5]) + "|" + str(mother[2]) + "}"
                
                if member[1]!=None:
                    sql = ("UPDATE {0}.members_v2 SET mother_id ="+str(mother[0]) +" WHERE name_id= '"+str(member[1])+"'").format(schema)
                elif member[2]!=None:
                    sql = ("UPDATE {0}.members_v2 SET mother_id ="+str(mother[0]) +" WHERE name_ch= '"+str(member[2])+"'").format(schema)
                elif member[3]!=None:
                    sql = ("UPDATE {0}.members_v2 SET mother_id ="+str(mother[0]) +" WHERE name_py= '"+str(member[3])+"'").format(schema)
                    
                input_log(cursor=cursor, schema = schema,ref = "MOTHER",logQuery = sql, desc = description)
                break
            
            elif member[5]!=None and member[5]==mother[3]:
                sql = ""
                description = "{py(member_mother|mother)=" + str(member[5]) + "|" + str(mother[3]) + "}"
                
                if member[1]!=None:
                    sql = ("UPDATE {0}.members_v2 SET mother_id ="+str(mother[0]) +" WHERE name_id= '"+str(member[1])+"'").format(schema)
                elif member[2]!=None:
                    sql = ("UPDATE {0}.members_v2 SET mother_id ="+str(mother[0]) +" WHERE name_ch= '"+str(member[2])+"'").format(schema)
                elif member[3]!=None:
                    sql = ("UPDATE {0}.members_v2 SET mother_id ="+str(mother[0]) +" WHERE name_py= '"+str(member[3])+"'").format(schema)
                    
                input_log(cursor=cursor, schema = schema,ref = "MOTHER",logQuery = sql, desc = description)
                break
    return True

def spouse_ref(schema,cursor,member_limit_from,member_limit_to):
    spouseQuery = "SELECT id, name_id, name_ch, name_py FROM {0}.members_v2 WHERE id >= {1} AND id < {2}".format(schema,member_limit_from,member_limit_to)
    spouses = execute_query(cursor, spouseQuery, get_result=True)
    
    getSpouseQuery = """
        SELECT m.id, m.name_id, m.name_ch, m.name_py, m.couple_id_name, m.couple_ch_name, m.couple_py_name  
        FROM {0}.members_v1 m
        WHERE 
        m.couple_id_name in (SELECT name_id from public.members_v2 f where f.id>={1} and f.id<={2})
        or
        m.couple_ch_name in (SELECT name_ch from public.members_v2 f where f.id>={1} and f.id<={2})
        or
        m.couple_ch_name in (SELECT name_py from public.members_v2 f where f.id>={1} and f.id<={2})""".format(schema,member_limit_from,member_limit_to)
    members = execute_query(cursor, getSpouseQuery, get_result=True)
    
    for member in members:
        for spouse in spouses:
            
            if member[4]!=None and member[4]==spouse[1]:
                sql = ""
                description = "{id(member_spouse|spouse)=" + str(member[4]) + "|" + str(spouse[1]) + "}"
                
                if member[1]!=None:
                    sql = ("UPDATE {0}.members_v2 SET spouse_id ="+str(spouse[0]) +" WHERE name_id= '"+str(member[1])+"'").format(schema)
                elif member[2]!=None:
                    sql = ("UPDATE {0}.members_v2 SET spouse_id ="+str(spouse[0]) +" WHERE name_ch= '"+str(member[2])+"'").format(schema)
                elif member[3]!=None:
                    sql = ("UPDATE {0}.members_v2 SET spouse_id ="+str(spouse[0]) +" WHERE name_py= '"+str(member[3])+"'").format(schema)
                    
                input_log(cursor=cursor, schema = schema,ref = "SPOUSE",logQuery = sql, desc = description)
                break
            
            elif member[5]!=None and member[5]==spouse[2]:
                sql = ""
                description = "{ch(member_spouse|spouse)=" + str(member[5]) + "|" + str(spouse[2]) + "}"
                
                if member[1]!=None:
                    sql = ("UPDATE {0}.members_v2 SET spouse_id ="+str(spouse[0]) +" WHERE name_id= '"+str(member[1])+"'").format(schema)
                elif member[2]!=None:
                    sql = ("UPDATE {0}.members_v2 SET spouse_id ="+str(spouse[0]) +" WHERE name_ch= '"+str(member[2])+"'").format(schema)
                elif member[3]!=None:
                    sql = ("UPDATE {0}.members_v2 SET spouse_id ="+str(spouse[0]) +" WHERE name_py= '"+str(member[3])+"'").format(schema)
                    
                input_log(cursor=cursor, schema = schema,ref = "SPOUSE",logQuery = sql, desc = description)
                break
            
            elif member[6]!=None and member[6]==spouse[3]:
                sql = ""
                description = "{py(member_spouse|spouse)=" + str(member[6]) + "|" + str(spouse[3]) + "}"
                
                if member[1]!=None:
                    sql = ("UPDATE {0}.members_v2 SET spouse_id ="+str(spouse[0]) +" WHERE name_id= '"+str(member[1])+"'").format(schema)
                elif member[2]!=None:
                    sql = ("UPDATE {0}.members_v2 SET spouse_id ="+str(spouse[0]) +" WHERE name_ch= '"+str(member[2])+"'").format(schema)
                elif member[3]!=None:
                    sql = ("UPDATE {0}.members_v2 SET spouse_id ="+str(spouse[0]) +" WHERE name_py= '"+str(member[3])+"'").format(schema)
                    
                input_log(cursor=cursor, schema = schema,ref = "SPOUSE",logQuery = sql, desc = description)
                break
    return True

def input_log(cursor,schema,ref,logQuery,desc):
    execute_query(
        cursor = cursor,
        query = "INSERT INTO {0}.member_update_log (ref_to,query_update,description,status) VALUES (%s,%s,%s,0);".format(schema),
        inputs = [ref,logQuery,desc],
        get_result=False
    )