"""
Layers:
1. psycopg2
2. googleapi
3. helpers
"""
import json
import boto3
import sys
import os
import traceback
import io
import helper as h
from db_helper import *
from datetime import datetime

MAP_MEMBERS = {
    "id":0,
    "date":1,
    "name_id":2,
    "name_ch":3,
    "name_py":4,
    "gender":5,
    "birthdate":6,
    "address":7,
    "city":8,
    "email":9,
    "father_id":10,
    "mother_id":11,
    "spouse_id":12,
    "num_children":13,
    "inputter_id":14,
    "collector_id":15,
    "from_xian_you":16,
    "is_deceased":17,
    "is_verified":18,
    "page_index":19,
    "created_at":20,
    "updated_at":21,
    "collected_at":22,
    "inputted_at":23,
    "updated_by":24,
    "num_siblings":25,
    "num_grandchildren":26
}

MAP_SPREADSHEET = {
    
}
        
def duplicate_checker(cursor,schema,name_id,name_ch,name_py, birthdate):
    baseSql = """SELECT {1}
        FROM {0}.members_v2 WHERE name_id=%s OR name_ch=%s OR name_py=%s OR birthdate=%s
        """.format(schema, ', '.join(MAP_MEMBERS.keys()))
        
    # print("duplicate : {}".format(baseSql));
    
    params = []
    if name_id != "":
        params.append(name_id)
    else:
        params.append(None)
    
        
    if name_ch != "":
        params.append(name_ch)
    else:
        params.append(None)
        
    if name_py != "":
        params.append(name_py)
    else:
        params.append(None)
        
    if name_py != "":
        params.append(birthdate)
    else:
        params.append(None)
        
    baseSql = baseSql.format(schema)
        
    return execute_query(cursor, baseSql, inputs = params, get_result=True)

def ambiguos_checker(cursor,schema,name_id,name_ch,name_py):
    baseSql = """
        SELECT {1} FROM {0}.members_v2 WHERE name_id=%s OR name_ch=%s OR name_py=%s
    """.format(schema, ', '.join(MAP_MEMBERS.keys()))
    
    # print("ambiguos : {}".format(baseSql));
    
    params = []
    if name_id != "":
        params.append(name_id)
    else:
        params.append(None)
    
        
    if name_ch != "":
        params.append(name_ch)
    else:
        params.append(None)
        
    if name_py != "":
        params.append(name_py)
    else:
        params.append(None)
        
    baseSql = baseSql.format(schema)
        
    return execute_query(cursor, baseSql, inputs = params, get_result=True)

def input_log(cursor,schema="public", data={}):
    data_keys = []
    data_value = []
    # from dictionary to array
    for dkey,dvalue in data.items():
        data_keys.append(dkey)
        data_value.append(dvalue)
        
    str = ",".join(data_keys)
    insert_str = "INSERT INTO {}.member_update_log"
    insert_str = insert_str + "(" + str + ") VALUES (" + ", ".join(['%s']*len(data_keys)) + ")"
    insert_str = insert_str.format(schema)
    
    execute_query(cursor, insert_str, inputs = data_value, get_result=False)

def update_query(cursor, schema, data_query, data, label = "GENERAL"):
    baseSql = "UPDATE {}.members_v2 SET"
    updateSql = ""
    params = []
    desc = []
    
    if data_query[MAP_MEMBERS["name_id"]]!=data[4] and len(data[4].lstrip().rstrip())>0:
        updateSql = updateSql + ", name_id = %s"
        params.append(data[4].lstrip().rstrip().title())
        desc.append({
            "name_id" : {
                "old" : data_query[MAP_MEMBERS["name_id"]],
                "new" : data[4].lstrip().rstrip().title(),
            }
        })
    if data_query[MAP_MEMBERS["name_ch"]]!=data[5] and len(data[5].replace(" ",""))>0:
        updateSql = updateSql + ", name_ch = %s"
        params.append(data[5].replace(" ",""))
        desc.append({
            "name_ch" : {
                "old" : data_query[MAP_MEMBERS["name_ch"]],
                "new" : data[5].replace(" ",""),
            }
        })
        
    if data_query[MAP_MEMBERS["name_py"]]!=data[6] and len(data[6].lstrip().rstrip())>0:
        updateSql = updateSql + ", name_py = %s"
        params.append(data[6].lstrip().rstrip().title())
        desc.append({
            "name_py" : {
                "old" : data_query[MAP_MEMBERS["name_py"]],
                "new" : data[6].lstrip().rstrip().title(),
            }
        })
    if data_query[MAP_MEMBERS["gender"]]!=data[7] and len(data[7].lstrip().rstrip())>0:
        updateSql = updateSql + ", gender = %s"

        strGender = 'Tidak Tahu 未知'
        if data[7].lstrip().rstrip().find('Pria') > -1:
            strGender = "Pria 男"
        elif data[7].lstrip().rstrip().find('Wanita') > -1:
            strGender = "Wanita 女"
        params.append(strGender)
        desc.append({
            "gender" : {
                "old" : data_query[MAP_MEMBERS["gender"]],
                "new" : strGender,
            }
        })
    if data_query[MAP_MEMBERS["birthdate"]]!=h.user_dob(data) and h.user_dob(data)!=None:
        updateSql = updateSql + ", birthdate = %s"
        params.append(h.user_dob(data))
        desc.append({
            "birthdate" : {
                "old" : str(data_query[MAP_MEMBERS["birthdate"]]),
                "new" : str(h.user_dob(data)),
            }
        })
        
    if data_query[MAP_MEMBERS["address"]]!=data[14] and len(data[14].lstrip().rstrip())>0:
        updateSql = updateSql + ", address = %s"
        params.append(data[14].lstrip().rstrip().title())
        desc.append({
            "address" : {
                "old" : data_query[MAP_MEMBERS["address"]],
                "new" : data[14].lstrip().rstrip().title(),
            }
        })
        
    if data_query[MAP_MEMBERS["city"]]!=data[29] and len(data[29].lstrip().rstrip())>0:
        updateSql = updateSql + ", city = %s"
        params.append(data[29].lstrip().rstrip().title())
        desc.append({
            "city" : {
                "old" : data_query[MAP_MEMBERS["city"]],
                "new" : data[29].lstrip().rstrip().title(),
            }
        })
        
    if len(params)>0:
        collector = h.collect_staff(schema,cursor, "COLLECTOR")
        if data_query[MAP_MEMBERS["collector_id"]]!=h.get_collector(collector,data) and h.get_collector(collector,data)!=None:
            updateSql = updateSql + ", collector_id = %s"
            params.append(h.get_collector(collector,data))
            desc.append({
                "collector_id" : {
                    "old" : str(data_query[MAP_MEMBERS["collector_id"]]),
                    "new" : str(h.get_collector(collector,data)),
                }
            })
            
        inputter = h.collect_staff(schema,cursor, "INPUTTER")
        if data_query[MAP_MEMBERS["inputter_id"]]!=h.get_inputter(inputter,data) and h.get_inputter(inputter,data)!=None:
            updateSql = updateSql + ", inputter_id = %s"
            params.append(h.get_inputter(inputter,data))
            desc.append({
                "inputter_id" : {
                    "old" : str(data_query[MAP_MEMBERS["inputter_id"]]),
                    "new" : str(h.get_inputter(inputter,data)),
                }
            })
        if data_query[MAP_MEMBERS["page_index"]]!=data[2] and len(data[2].lstrip().rstrip())>0:
            updateSql = updateSql + ", page_index = %s"
            params.append(data[2].lstrip().rstrip())
            desc.append({
                "page_index" : {
                    "old" : str(data_query[MAP_MEMBERS["page_index"]]),
                    "new" : data[2].lstrip().rstrip(),
                }
            })
            
        sql =  baseSql + updateSql[1:] + " WHERE id=" +str(data_query[0])
        sql = sql.format(schema)
        input_log(cursor=cursor,schema=schema, data={
            "ref_to" : label + " | "+str(data_query[0]),
            "query_update" : sql,
            "description" : json.dumps(desc),
            "status" : 1
        })
        
        execute_query(cursor = cursor, query = sql, inputs = params, get_result=False)
        
def update_query_spouse(cursor, schema, data_query, data, label = "GENERAL_SP"):
    baseSql = "UPDATE {}.members_v2 SET"
    updateSql = ""
    params = []
    desc = []
    
    if data_query[MAP_MEMBERS["name_id"]]!=data[15] and len(data[15].lstrip().rstrip())>0:
        updateSql = updateSql + ", name_id = %s"
        params.append(data[15].lstrip().rstrip().title())
        desc.append({
            "name_id" : {
                "old" : data_query[MAP_MEMBERS["name_id"]],
                "new" : data[15].lstrip().rstrip().title(),
            }
        })
    if data_query[MAP_MEMBERS["name_ch"]]!=data[16] and len(data[16].replace(" ",""))>0:
        updateSql = updateSql + ", name_ch = %s"
        params.append(data[16].replace(" ",""))
        desc.append({
            "name_ch" : {
                "old" : data_query[MAP_MEMBERS["name_ch"]],
                "new" : data[16].replace(" ",""),
            }
        })
        
    if data_query[MAP_MEMBERS["name_py"]]!=data[17] and len(data[17].lstrip().rstrip())>0:
        updateSql = updateSql + ", name_py = %s"
        params.append(data[17].lstrip().rstrip().title())
        desc.append({
            "name_py" : {
                "old" : data_query[MAP_MEMBERS["name_py"]],
                "new" : data[17].lstrip().rstrip().title(),
            }
        })
    if data_query[MAP_MEMBERS["birthdate"]]!=h.couple_dob(data) and h.couple_dob(data)!=None:
        updateSql = updateSql + ", birthdate = %s"
        params.append(h.couple_dob(data))
        desc.append({
            "birthdate" : {
                "old" : str(data_query[MAP_MEMBERS["birthdate"]]),
                "new" : str(h.couple_dob(data)),
            }
        })
        
    if data_query[MAP_MEMBERS["address"]]!=data[31] and len(data[31].lstrip().rstrip())>0:
        updateSql = updateSql + ", address = %s"
        params.append(data[31].lstrip().rstrip().title())
        desc.append({
            "address" : {
                "old" : data_query[MAP_MEMBERS["address"]],
                "new" : data[31].lstrip().rstrip().title(),
            }
        })
        
    if data_query[MAP_MEMBERS["city"]]!=data[33] and len(data[33].lstrip().rstrip())>0:
        updateSql = updateSql + ", city = %s"
        params.append(data[33].lstrip().rstrip().title())
        desc.append({
            "city" : {
                "old" : data_query[MAP_MEMBERS["city"]],
                "new" : data[33].lstrip().rstrip().title(),
            }
        })
        
    if len(params)>0:
        collector = h.collect_staff(schema,cursor, "COLLECTOR")
        if data_query[MAP_MEMBERS["collector_id"]]!=h.get_collector(collector,data) and h.get_collector(collector,data)!=None:
            updateSql = updateSql + ", collector_id = %s"
            params.append(h.get_collector(collector,data))
            desc.append({
                "collector_id" : {
                    "old" : str(data_query[MAP_MEMBERS["collector_id"]]),
                    "new" : str(h.get_collector(collector,data)),
                }
            })
            
        inputter = h.collect_staff(schema,cursor, "INPUTTER")
        if data_query[MAP_MEMBERS["inputter_id"]]!=h.get_inputter(inputter,data) and h.get_inputter(inputter,data)!=None:
            updateSql = updateSql + ", inputter_id = %s"
            params.append(h.get_inputter(inputter,data))
            desc.append({
                "inputter_id" : {
                    "old" : str(data_query[MAP_MEMBERS["inputter_id"]]),
                    "new" : str(h.get_inputter(inputter,data)),
                }
            })
            
        if data_query[MAP_MEMBERS["page_index"]]!=data[2] and len(data[2].lstrip().rstrip())>0:
            updateSql = updateSql + ", page_index = %s"
            params.append(data[2].lstrip().rstrip())
            desc.append({
                "page_index" : {
                    "old" : str(data_query[MAP_MEMBERS["page_index"]]),
                    "new" : data[2].lstrip().rstrip(),
                }
            })
            
        sql =  baseSql + updateSql[1:] + " WHERE id=" +str(data_query[0])
        sql = sql.format(schema)
        input_log(cursor=cursor,schema=schema, data={
            "ref_to" : label + " | "+str(data_query[0]),
            "query_update" : sql,
            "description" : json.dumps(desc),
            "status" : 1
        })
        
        execute_query(cursor = cursor, query = sql, inputs = params, get_result=False)
        
def lambda_handler(event, context):
    member_data = h.fetch_api(event['Batches'][0])
    try:
        cnx = make_connection(event['DBConnection']['endpoint'],
                              event['DBConnection']['port'],
                              event['DBConnection']['dbuser'],
                              event['DBConnection']['dbpassword'],
                              event['DBConnection']['database'])
        cursor=cnx.cursor()
        logger.info('Connected!')
        
        countQuery = 'select count(*) from {}.members_v2'.format(event['env'])
        cursor.execute(countQuery)
        records = cursor.fetchone()[0]
        
        # print(member_data)
        # Insert into members_v1 and members_v2
        for data in member_data:
            if len(data) <= 41:
                while len(data) <= 41:
                    data.append("")
                    
            staff = h.collect_staff(event['env'],cursor, "INPUTTER")    
            collector = h.collect_staff(event['env'],cursor, "COLLECTOR")
            
            data_member_v1 = {}
            data_member_v2 = {}
            data_spouse_v2 = {}
            
            # print("TEST", data[0])
            raw_timestamp = data[0].split("/")
            if len(raw_timestamp) ==3:
                month = raw_timestamp[0]
                date = raw_timestamp[1]
                other = raw_timestamp[2]
                other = other.split(" ")
                year = other[0]
                other = other[1].split(":")
                hour = other[0]
                minute = other[1]
                second = other[2]
                if len(month)==1:
                    month = "0"+month
                if len(date)==1:
                    date = "0"+date
                    
                data_member_v1.update(date=year+"-"+month+"-"+date+" "+hour+":"+minute+":"+second)
                data_member_v2.update(date=year+"-"+month+"-"+date+" "+hour+":"+minute+":"+second)
                data_spouse_v2.update(date=year+"-"+month+"-"+date+" "+hour+":"+minute+":"+second)
                data_member_v2.update(inputted_at=year+"-"+month+"-"+date+" "+hour+":"+minute+":"+second)
            
            data_member_v2.update(updated_at=datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            data_member_v2.update(created_at=datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            # trigger name_id
            if len(data[4].lstrip().rstrip()) > 0:
                data_member_v1.update(name_id=data[4].lstrip().rstrip().title())
                data_member_v2.update(name_id=data[4].lstrip().rstrip().title())
            # trigger name_ch
            if len(data[5].replace(" ","")) > 0:
                data_member_v1.update(name_ch=data[5].replace(" ",""))
                data_member_v2.update(name_ch=data[5].replace(" ",""))
            # trigger name_py
            if len(data[6].lstrip().rstrip()) > 0:
                data_member_v1.update(name_py=data[6].lstrip().rstrip().title())
                data_member_v2.update(name_py=data[6].lstrip().rstrip().title())
            # trigger gender
            strGender = 'Tidak Tahu 未知'
            if data[7].lstrip().rstrip().find('Pria') > -1:
                strGender = "Pria 男"
            elif data[7].lstrip().rstrip().find('Wanita') > -1:
                strGender = "Wanita 女"
            data_member_v1.update(gender=strGender)
            data_member_v2.update(gender=strGender)
            # trigger couple_gender
            strGender = 'Tidak Tahu 未知'
            if h.couple_gender(data).find('Pria') > -1:
                strGender = "Pria 男"
            elif h.couple_gender(data).find('Wanita') > -1:
                strGender = "Wanita 女"
            data_spouse_v2.update(gender=strGender)
            # trigger birth
            if h.user_dob(data)!=None:
                data_member_v1.update(birth=h.user_dob(data))
                data_member_v2.update(birthdate=h.user_dob(data))
            # trigger couple_birth
            if h.couple_dob(data)!=None:
                data_member_v1.update(couple_birth=h.couple_dob(data))
                data_spouse_v2.update(birthdate=h.couple_dob(data))
            # trigger address
            if len(data[14].lstrip().rstrip()) > 0:
                data_member_v1.update(address=data[14].lstrip().rstrip().title())
                data_member_v2.update(address=data[14].lstrip().rstrip().title())
            # trigger couple address
            if len(data[31].lstrip().rstrip()) > 0:
                data_member_v1.update(couple_address=data[31].lstrip().rstrip().title())
                data_spouse_v2.update(address=data[31].lstrip().rstrip().title())
            # trigger city
            if len(data[29].lstrip().rstrip()) > 0:
                data_member_v1.update(city=data[29].lstrip().rstrip().title())
                data_member_v2.update(city=data[29].lstrip().rstrip().title())
            else:
                data_member_v1.update(city="Tidak Tahu")
                data_member_v2.update(city="Tidak Tahu")
            # trigger couple city
            if len(data[33].lstrip().rstrip()) > 0:
                data_member_v1.update(couple_city=data[33].lstrip().rstrip().title())
                data_spouse_v2.update(city=data[33].lstrip().rstrip().title())
            else:
                data_member_v1.update(couple_city="Tidak Tahu")
                data_spouse_v2.update(city="Tidak Tahu")
            # trigger phone
            if len(data[13].lstrip().rstrip()) > 0:
                data_member_v1.update(phone=data[13].lstrip().rstrip().title())
                
            # trigger father_id_name
            if len(data[9].lstrip().rstrip()) > 0:
                data_member_v1.update(father_id_name=data[9].lstrip().rstrip().title())
            # trigger father_ch_name
            if len(data[10].replace(" ","")) > 0:
                data_member_v1.update(father_ch_name=data[10].replace(" ",""))
            # trigger father_birth
            if h.father_dob(data)!=None:
                data_member_v1.update(father_birth=h.father_dob(data))
                
            # trigger mother_id_name
            if len(data[11].lstrip().rstrip()) > 0:
                data_member_v1.update(mother_id_name=data[11].lstrip().rstrip().title())
            # trigger mother_ch_name
            if len(data[12].replace(" ","")) > 0:
                data_member_v1.update(mother_ch_name=data[12].replace(" ",""))
            # trigger mother_birth
            if h.mother_dob(data)!=None:
                data_member_v1.update(mother_birth=h.mother_dob(data))
                
            # trigger couple_id_name
            if len(data[15].lstrip().rstrip()) > 0:
                data_member_v1.update(couple_id_name=data[15].lstrip().rstrip().title())
                data_spouse_v2.update(name_id=data[15].lstrip().rstrip().title())
            # trigger couple_ch_name
            if len(data[16].replace(" ","")) > 0:
                data_member_v1.update(couple_ch_name=data[16].replace(" ",""))
                data_spouse_v2.update(name_ch=data[16].replace(" ",""))
            # trigger couple_py_name
            if len(data[17].lstrip().rstrip()) > 0:
                data_member_v1.update(couple_py_name=data[17].lstrip().rstrip().title())
                data_spouse_v2.update(name_py=data[17].lstrip().rstrip().title())
                
            # trigger from_xian_you
            data_member_v1.update(from_xian_you=h.is_xian_you(data[22]))
            data_member_v2.update(from_xian_you=h.is_xian_you(data[22]))
            data_spouse_v2.update(from_xian_you=h.is_xian_you(data[23]))
            # trigger inputter
            data_member_v1.update(inputter_id=h.get_inputter(staff, data))
            data_member_v2.update(inputter_id=h.get_inputter(staff, data))
            data_spouse_v2.update(inputter_id=h.get_inputter(staff, data))
            # trigger collector
            data_member_v1.update(collector_id=h.get_collector(collector, data))
            data_member_v2.update(collector_id=h.get_collector(collector, data))
            data_spouse_v2.update(collector_id=h.get_collector(collector, data))
            # trigger page_index
            data_member_v1.update(page_index=data[2])
            data_member_v2.update(page_index=data[2])
            data_spouse_v2.update(page_index=data[2])
            # trigger deceased
            data_member_v1.update(is_deceased = h.get_deceased_status(data[40]))
            data_member_v2.update(is_deceased = h.get_deceased_status(data[40]))
            data_spouse_v2.update(is_deceased = h.get_deceased_status(data[41]))
            #trigger number_of_child
            if len(data[25].rstrip().lstrip())<=0:
                data_member_v1.update(number_of_child = 0)
                data_member_v2.update(num_children = 0)
                data_spouse_v2.update(num_children = 0)
            else:
                data_member_v1.update(number_of_child = data[25])
                data_member_v2.update(num_children = data[25])
                data_spouse_v2.update(num_children = data[25])
            insert_string = h.insert_member_v1(
                cursor = cursor,
                schema = event['env'], 
                data   = data_member_v1
            )
            
            member_id = 0
            
            userDuplicate = duplicate_checker(
                cursor  = cursor,
                schema  = event['env'],
                name_id = data[4].lstrip().rstrip().title(),
                name_ch = data[5].replace(" ",""), 
                name_py = data[6].lstrip().rstrip().title(),
                birthdate = h.user_dob(data)
            )
            if userDuplicate == []:
                userAmbiguos = ambiguos_checker(
                    cursor  = cursor,
                    schema  = event['env'],
                    name_id = data[4].lstrip().rstrip().title(),
                    name_ch = data[5].replace(" ",""), 
                    name_py = data[6].lstrip().rstrip().title(),
                )
                if userAmbiguos != []:
                    h.ambiguos_member_v2(
                        cursor = cursor,
                        schema = event['env'],
                        old    = userAmbiguos[0],
                        found  = data,
                        inputter_id = h.get_inputter(staff, data),
                        collector_id = h.get_collector(collector, data),
                        desc_input = "member_row"
                    )
                
                # print(data_member_v2)
                
                result_member_v2 = h.insert_member_v2(
                    cursor = cursor,
                    schema = event['env'],
                    data   = data_member_v2
                )
                member_id = result_member_v2[0][0]
                
                if data[13]!="":
                    insert_string=h.insert_phone_number(
                        cursor = cursor,
                        schema = event['env'],
                        member_id = member_id,
                        phones=data[13]
                    )
                    
            else:
                duplicate_member_v2 = h.duplicate_member_v2(
                    cursor = cursor,
                    schema = event['env'],
                    old    = userDuplicate[0],
                    found  = data,
                    desc_input = "member_row"
                )
                update_query(cursor=cursor,schema=event['env'],data_query = userDuplicate[0],data = data)
        
                
            if len(data[15]) > 0 or len(data[16]) > 0 or len(data[17]) > 0:
                if member_id!=0:
                    data_spouse_v2.update(spouse_id = member_id)
                
                userDuplicate = duplicate_checker(
                    cursor  = cursor,
                    schema  = event['env'],
                    name_id = data[15].lstrip().rstrip().title(),
                    name_ch = data[16].replace(" ",""), 
                    name_py = data[17].lstrip().rstrip().title(),
                    birthdate = h.couple_dob(data)
                )
                if userDuplicate == []:
                    userAmbiguos = ambiguos_checker(
                        cursor  = cursor,
                        schema  = event['env'],
                        name_id = data[15].lstrip().rstrip().title(),
                        name_ch = data[16].replace(" ",""), 
                        name_py = data[17].lstrip().rstrip().title()
                    )
                    if userAmbiguos != []:
                        h.ambiguos_member_v2(
                            cursor = cursor,
                            schema = event['env'],
                            old    = userAmbiguos[0],
                            found  = data,
                            desc_input = "member_spouse",
                            inputter_id = h.get_inputter(staff, data),
                            collector_id = h.get_collector(collector, data)
                        )
                    result_spouse_v2 = h.insert_member_v2(
                        cursor = cursor,
                        schema = event['env'],
                        data   = data_spouse_v2
                    )
                    
                    if data[30]!="":
                        member_spouse_id = result_spouse_v2[0][0]
                        insert_string=h.insert_phone_number(
                            cursor = cursor,
                            schema = event['env'],
                            member_id = member_spouse_id,
                            phones=data[30]
                        )
                else:
                    duplicate_member_v2 = h.duplicate_spouse_v2(
                        cursor = cursor,
                        schema = event['env'],
                        old    = userDuplicate[0],
                        found  = data
                    )
                    update_query_spouse(cursor=cursor,schema=event['env'],data_query = userDuplicate[0],data = data, label="GENERAL_SP")
                
                            
        # countQuery = 'select count(*) from {}.members_v2'.format(event['env'])
        # cursor.execute(countQuery)
        # records = cursor.fetchone()[0]
        
        return {
            "Status" : True,
            "NumRows" : records
        }
    except:
        return log_err("ERROR: Cannot connect to database from handler.\n{}".format(traceback.format_exc()))