import sys
import json
import re
from db_helper import *

def get_deceased_status(str):
    if str.find("Almarhum") > -1:
        return True
    return False

def is_xian_you(str):
    if str.find("Ya") > -1:
        return True
    return False

def user_dob(data):
    if data[21]=="" or data[20]=="" or data[8]=="":
        return None
        
    month = "0" + data[21][:1]
    if data[21]=="10 - Oktober":
        month = '10'
    elif data[21]=="11 - November":
        month = '11'
    elif data[21]=="12 - Desember":
        month = '12'
        
    if int(data[20].strip())<10:
        return data[8]+"-"+month+"-0"+data[20]
    else:
        return data[8]+"-"+month+"-"+data[20]
    
def couple_gender(data):
    if data[7].find('Pria') > -1:
        return "Wanita 女"
    elif data[7].find('Wanita') > -1:
        return "Pria 男"
    return "Tidak Tahu 未知"
    
def couple_dob(data):
    if data[28]=="" or data[27]=="" or data[28]=="":
        return None
        
    month = "0" + data[28][:1]
    if data[28]=="10 - Oktober":
        month = '10'
    elif data[28]=="11 - November":
        month = '11'
    elif data[28]=="12 - Desember":
        month = '12'
        
    if int(data[27].strip())<10:
        return data[18]+"-"+month+"-0"+data[27]
    else:
        return data[18]+"-"+month+"-"+data[27]
        
def father_dob(data):
    if data[34]=="" or data[35]=="" or data[36]=="":
        return None
        
    month = "0" + data[35][:1]
    if data[35]=="10 - Oktober":
        month = '10'
    elif data[35]=="11 - November":
        month = '11'
    elif data[35]=="12 - Desember":
        month = '12'
        
    if int(data[34].strip())<10:
        return data[36]+"-"+month+"-0"+data[34]
    else:
        return data[36]+"-"+month+"-"+data[34]

def mother_dob(data):
    if data[37]=="" or data[38]=="" or data[39]=="":
        return None
        
    month = "0" + data[38][:1]
    if data[38]=="10 - Oktober":
        month = '10'
    elif data[38]=="11 - November":
        month = '11'
    elif data[38]=="12 - Desember":
        month = '12'
        
    if int(data[37].strip())<10:
        return data[39]+"-"+month+"-0"+data[37]
    else:
        return data[39]+"-"+month+"-"+data[37]

def collect_staff(schema,cursor,role=None):
    inputter_data = "select * from {0}.staff".format(schema)
    if role!=None:
        inputter_data = "select * from {0}.staff where role='{1}'".format(schema,role)    
    cursor.execute(inputter_data)

    records = cursor.fetchall() 
    return records

def get_inputter(staffs, data):
    for staff in staffs:
        if data[19]==None:
            return None
        else:
            if data[19]!="":
                if staff[1]!=None:
                    if data[19].find(staff[1])!=-1:
                        return staff[0]
                if staff[2]!=None:
                    if data[19].find(staff[2])!=-1:
                        return staff[0]
                if staff[3]!=None:
                    if data[19].find(staff[3])!=-1:
                        return staff[0]
    return None

def get_collector(staffs, data):
    for staff in staffs:
        if data[1]==None:
            return None
        else:
            if data[1]!="":
                if staff[1]!=None:
                    if data[1].find(staff[1])!=-1:
                        return staff[0]
                if staff[2]!=None:
                    if data[1].find(staff[2])!=-1:
                        return staff[0]
                if staff[3]!=None:
                    if data[1].find(staff[3])!=-1:
                        return staff[0]
    return None
    
def insert_member_v1(cursor,schema="public", data={}):
    data_keys = []
    data_value = []
    # from dictionary to array
    for dkey,dvalue in data.items():
        data_keys.append(dkey)
        data_value.append(dvalue)
        
    str = ",".join(data_keys)
    insert_str = "INSERT INTO {}.members_v1"
    insert_str = insert_str + "(" + str + ") VALUES (" + ", ".join(['%s']*len(data_keys)) + ") RETURNING id"
    insert_str = insert_str.format(schema)
    
    execute_query(cursor, insert_str, inputs = data_value, get_result=False)

def insert_member_v2(cursor,schema="public", data={}):
    data_keys = []
    data_value = []
    # from dictionary to array
    for dkey,dvalue in data.items():
        data_keys.append(dkey)
        data_value.append(dvalue)
        
    str = ",".join(data_keys)
    insert_str = "INSERT INTO {}.members_v2"
    insert_str = insert_str + "(" + str + ") VALUES (" + ", ".join(['%s']*len(data_keys)) + ") RETURNING id"
    insert_str = insert_str.format(schema)
        
    cursor.execute(insert_str, data_value)
    id = cursor.fetchone()[0]
    return id
    
def ambiguos_member_v2(cursor,schema="public", old={}, found=[], inputter_id="", collector_id="", desc_input="member"):
    baseSql = "SELECT * FROM {0}.member_ambiguos WHERE id={1}".format(schema, str(old[0]))
    val = execute_query(cursor, baseSql, get_result=True)
    if val==[]:
        baseSql = "INSERT INTO {0}.member_ambiguos(member_id, name_id, name_ch, name_py, gender, birthdate, address, city, desc_input,collector_id,inputter_id) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)".format(schema)
        old_params = [old[0],old[2],old[3],old[4],old[5],old[6],old[7],old[8],'1st_input',old[14],old[15]]
        execute_query(cursor, baseSql, inputs = old_params, get_result=False)
        
    baseSql = "INSERT INTO {0}.member_ambiguos(member_id, name_id, name_ch, name_py, gender, birthdate, address, city, desc_input,collector_id,inputter_id) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)".format(schema)
    if desc_input=="member_row":
        name_id=None
        if len(found[4].lstrip().rstrip()) > 0:
            name_id=found[4].lstrip().rstrip().title()
        
        name_ch=None
        if len(found[5].replace(" ","")) > 0:
            name_ch=found[5].replace(" ","")
            
        name_py=None
        if len(found[6].lstrip().rstrip()) > 0:
            name_py=found[6].lstrip().rstrip().title()
    
        gender = 'Tidak Tahu 未知'
        if found[7].lstrip().rstrip().find('Pria') > -1:
            gender = "Pria 男"
        elif found[7].lstrip().rstrip().find('Wanita') > -1:
            gender = "Wanita 女"
            
        birth = None
        if user_dob(found)!=None:
            birth=user_dob(found)
        
        address = None
        if len(found[14].lstrip().rstrip()) > 0:
            address=found[14].lstrip().rstrip().title()
            
        city = "Tidak Tahu"
        if len(found[29].lstrip().rstrip()) > 0:
            city=found[29].lstrip().rstrip().title()
    else:
        name_id=None
        if len(found[15].lstrip().rstrip()) > 0:
            name_id=found[15].lstrip().rstrip().title()
        
        name_ch=None
        if len(found[16].replace(" ","")) > 0:
            name_ch=found[16].replace(" ","")
            
        name_py=None
        if len(found[17].lstrip().rstrip()) > 0:
            name_py=found[17].lstrip().rstrip().title()
    
        gender = 'Tidak Tahu 未知'
        if found[7].lstrip().rstrip().find('Pria') > -1:
            gender = "Wanita 女"
        elif found[7].lstrip().rstrip().find('Wanita') > -1:
            gender = "Pria 男"
            
        birth = None
        if couple_dob(found)!=None:
            birth=couple_dob(found)
        
        address = None
        if len(found[31].lstrip().rstrip()) > 0:
            address=found[31].lstrip().rstrip().title()
            
        city = "Tidak Tahu"
        if len(found[33].lstrip().rstrip()) > 0:
            city=found[33].lstrip().rstrip().title()
    
    getIdSql = "SELECT max(id)+1 FROM {0}.members_v2".format(schema)
    val = execute_query(cursor, getIdSql, get_result=True)
    insert_id = val[0][0]
    
    found_params = [insert_id,name_id, name_ch, name_py, gender,birth,address,city,desc_input,inputter_id,collector_id]
    execute_query(cursor, baseSql, inputs = found_params, get_result=False)
    
def duplicate_member_v2(cursor,schema="public", old={}, found=[], desc_input="member_row"):
    baseSql = "SELECT * FROM {0}.member_duplicate WHERE id={1}".format(schema, str(old[0]))
    val = execute_query(cursor, baseSql, get_result=True)
    if val==[]:
        baseSql = "INSERT INTO {0}.member_duplicate(member_id, name_id, name_ch, name_py, gender, birthdate, address, city, desc_input) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)".format(schema)
        old_params = [old[0],old[2],old[3],old[4],old[5],old[6],old[7],old[8],'1st_input']
        execute_query(cursor, baseSql, inputs = old_params, get_result=False)
        
    name_id=None
    if len(found[4].lstrip().rstrip()) > 0:
        name_id=found[4].lstrip().rstrip().title()
    
    name_ch=None
    if len(found[5].replace(" ","")) > 0:
        name_ch=found[5].replace(" ","")
        
    name_py=None
    if len(found[6].lstrip().rstrip()) > 0:
        name_py=found[6].lstrip().rstrip().title()

    gender = 'Tidak Tahu 未知'
    if found[7].lstrip().rstrip().find('Pria') > -1:
        gender = "Pria 男"
    elif found[7].lstrip().rstrip().find('Wanita') > -1:
        gender = "Wanita 女"
        
    birth = None
    if user_dob(found)!=None:
        birth=user_dob(found)
    
    address = None
    if len(found[14].lstrip().rstrip()) > 0:
        address=found[14].lstrip().rstrip().title()
        
    city = "Tidak Tahu"
    if len(found[29].lstrip().rstrip()) > 0:
        city=found[29].lstrip().rstrip().title()
    
    found_params = [old[0],name_id, name_ch, name_py, gender,birth,address,city,desc_input]
    execute_query(cursor, baseSql, inputs = found_params, get_result=False)

def duplicate_spouse_v2(cursor,schema="public", old={}, found=[], desc_input="member_spouse"):
    baseSql = "SELECT * FROM {0}.member_duplicate WHERE id={1}".format(schema, str(old[0]))
    val = execute_query(cursor, baseSql, get_result=True)
    if val==[]:
        baseSql = "INSERT INTO {0}.member_duplicate(member_id, name_id, name_ch, name_py, gender, birthdate, address, city, desc_input) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)".format(schema)
        old_params = [old[0],old[2],old[3],old[4],old[5],old[6],old[7],old[8],'1st_input']
        execute_query(cursor, baseSql, inputs = old_params, get_result=False)
        
    name_id=None
    if len(found[15].lstrip().rstrip()) > 0:
        name_id=found[15].lstrip().rstrip().title()
    
    name_ch=None
    if len(found[16].replace(" ","")) > 0:
        name_ch=found[16].replace(" ","")
        
    name_py=None
    if len(found[17].lstrip().rstrip()) > 0:
        name_py=found[17].lstrip().rstrip().title()

    gender = 'Tidak Tahu 未知'
    if found[7].lstrip().rstrip().find('Pria') > -1:
        gender = "Wanita 女"
    elif found[7].lstrip().rstrip().find('Wanita') > -1:
        gender = "Pria 男"
        
    birth = None
    if couple_dob(found)!=None:
        birth=couple_dob(found)
    
    address = None
    if len(found[31].lstrip().rstrip()) > 0:
        address=found[31].lstrip().rstrip().title()
        
    city = "Tidak Tahu"
    if len(found[33].lstrip().rstrip()) > 0:
        city=found[33].lstrip().rstrip().title()
    
    found_params = [old[0],name_id, name_ch, name_py, gender,birth,address,city,desc_input]
    execute_query(cursor, baseSql, inputs = found_params, get_result=False)
    
def fetch_api(api="https://sheets.googleapis.com/v4/spreadsheets/1NTpdlZI3o2eYha-EU2GyBQjnSQWSb2_a2d6mxBEcWq8/values:batchGet?ranges=E2:AZ500&key=AIzaSyAOoxW0AGjlMAbBUS7Z89sP3QHyKeZNK10"):
    if sys.version_info[0] == 3:
        from urllib.request import urlopen
    else:
        from urllib import urlopen
        
    with urlopen(api) as url:
        s = url.read()

    json_data = json.loads(s)
    
    return json_data["valueRanges"][0]["values"]

def insert_phone_number(cursor,schema, member_id, phones):
    insert_rows = []
    phones = phones.replace(" ","")
    arrPhones = get_numbers(phones)
    for phone in arrPhones:
        if len(phone) > 0:
            strQuery = "INSERT INTO {0}.phone_numbers (member_id,value) VALUES (%s,%s);".format(schema)
            params = [member_id,phone]
            execute_query(cursor = cursor, query = strQuery, inputs = params, get_result=False)
        
def get_numbers(nums):
  result = re.split('[\/\-\*\－\,\，]', nums)
  numbers = []
  passnext = False
  for i, x in enumerate(result):
    if passnext:
      passnext = False
    else:
      if i < len(result)-1:
        if len(x) > 5:
          numbers.append(x)
        else:
          numbers.append('{}{}'.format(x, result[i+1]))
          passnext = True
      else:
        numbers.append(x)
  return(numbers)
    