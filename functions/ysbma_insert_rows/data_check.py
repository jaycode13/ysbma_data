import json
import traceback
import io
import boto3
import sys
import os
from db_helper import *

def lambda_handler(event, context):
    try:
        cnx = make_connection(event['DBConnection']['endpoint'],
                              event['DBConnection']['port'],
                              event['DBConnection']['dbuser'],
                              event['DBConnection']['dbpassword'],
                              event['DBConnection']['database'])
        cursor=cnx.cursor()
        logger.info('Connected!')
    except:
        return log_err("ERROR: Cannot connect to database from handler.\n{}".format(traceback.format_exc()))
    
    select_Query = "select count(id) from {}.members_v2".format(event["env"])
    cursor.execute(select_Query)
    records = cursor.fetchone()
    
    return records
