"""
Layers:
1. googleapi
"""

import json
import boto3
import sys
import os

def lambda_handler(event, context):
    access = event['Access']
    if len(access)==0:
        log_err("ERROR: Cannot connect to database from handler")
    
    api = os.environ.get('GOOGLE_API_GET_INFORMATION')
    #api = "https://sheets.googleapis.com/v4/spreadsheets/1NTpdlZI3o2eYha-EU2GyBQjnSQWSb2_a2d6mxBEcWq8/values:batchGet?ranges=E2:AQ500&key=AIzaSyAOoxW0AGjlMAbBUS7Z89sP3QHyKeZNK10"
    
    if sys.version_info[0] == 3:
        from urllib.request import urlopen
    else:
        from urllib import urlopen
        
    with urlopen(api) as url:
        s = url.read()

    json_data = json.loads(s)
    
    #return json_data
    
    sheets = json_data["sheets"]
    rowCount=sheets[0]['properties']['gridProperties']['rowCount']
    
    return { 
        "DBConnection": {
            "endpoint":     os.environ.get('ENDPOINT'),
            "port":         os.environ.get('PORT'),
            "dbuser":       os.environ.get('DBUSER'),
            "dbpassword":   os.environ.get('DBPASSWORD'),
            "database":     os.environ.get('DATABASE')  
        },
        'Rows' : rowCount,
        "Access": access,
        "env" : event['env'],
        'GoogleApi':{
            'Information'    : os.environ.get('GOOGLE_API_GET_INFORMATION'),
            'ApiKey'         : os.environ.get('GOOGLE_API_KEY'),
            'SpreadsheetId'  : os.environ.get('GOOGLE_SPREADSHEET_ID')
        }
    }
    