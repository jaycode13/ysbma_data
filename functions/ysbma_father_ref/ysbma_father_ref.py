"""
Layers:
1. psycopg2
2. helpers
"""
import json
import os
import traceback
import io
from db_helper import *

def lambda_handler(event, context):
    try:
        cnx = make_connection(event['DBConnection']['endpoint'],
                              event['DBConnection']['port'],
                              event['DBConnection']['dbuser'],
                              event['DBConnection']['dbpassword'],
                              event['DBConnection']['database'])
        cursor=cnx.cursor()
        logger.info('Connected!')
    except:
        return log_err("ERROR: Cannot connect to database from handler.\n{}".format(traceback.format_exc()))
        
    batcher = json.loads(event["Batches"][0])
    bfrom = batcher["from"]
    bto   = batcher["to"]
    
    father_ref(
        schema = event["env"],
        cursor = cursor,
        member_limit_from = bfrom,
        member_limit_to = bto
    )
        
    return True
    
def father_ref(schema,cursor,member_limit_from,member_limit_to):
    fatherQuery = "SELECT id, name_id, name_ch, name_py FROM {0}.members_v2 WHERE id >= {1} AND id < {2}".format(schema,member_limit_from,member_limit_to)
    fathers = execute_query(cursor, fatherQuery, get_result=True)
    
    getFatherQuery = """
        SELECT m.id, m.name_id, m.name_ch, m.name_py, m.father_id_name, m.father_ch_name 
        FROM {0}.members_v1 m
        WHERE 
        m.father_id_name in (SELECT name_id from public.members_v2 f where f.id>={1} and f.id<={2})
        or
        m.father_ch_name in (SELECT name_ch from public.members_v2 f where f.id>={1} and f.id<={2})
        or
        m.father_ch_name in (SELECT name_py from public.members_v2 f where f.id>={1} and f.id<={2})""".format(schema,member_limit_from,member_limit_to)
    members = execute_query(cursor, getFatherQuery, get_result=True)
    # print(members)
    
    for member in members:
        for father in fathers:
            
            if member[4]!=None and member[4]==father[1]:
                sql = ""
                description = "{id(member_father|father)=" + str(member[4]) + "|" + str(father[1]) + "}"
                
                if member[1]!=None:
                    sql = ("UPDATE {0}.members_v2 SET father_id ="+str(father[0]) +" WHERE name_id= '"+str(member[1])+"'").format(schema)
                elif member[2]!=None:
                    sql = ("UPDATE {0}.members_v2 SET father_id ="+str(father[0]) +" WHERE name_ch= '"+str(member[2])+"'").format(schema)
                elif member[3]!=None:
                    sql = ("UPDATE {0}.members_v2 SET father_id ="+str(father[0]) +" WHERE name_py= '"+str(member[3])+"'").format(schema)
                    
                input_log(cursor=cursor, schema = schema,ref = "FATHER",logQuery = sql, desc = description)
                break
            
            elif member[5]!=None and member[5]==father[2]:
                sql = ""
                description = "{ch(member_father|father)=" + str(member[5]) + "|" + str(father[2]) + "}"
                
                if member[1]!=None:
                    sql = ("UPDATE {0}.members_v2 SET father_id ="+str(father[0]) +" WHERE name_id= '"+str(member[1])+"'").format(schema)
                elif member[2]!=None:
                    sql = ("UPDATE {0}.members_v2 SET father_id ="+str(father[0]) +" WHERE name_ch= '"+str(member[2])+"'").format(schema)
                elif member[3]!=None:
                    sql = ("UPDATE {0}.members_v2 SET father_id ="+str(father[0]) +" WHERE name_py= '"+str(member[3])+"'").format(schema)
                    
                input_log(cursor=cursor, schema = schema,ref = "FATHER",logQuery = sql, desc = description)
                break
            
            elif member[5]!=None and member[5]==father[3]:
                sql = ""
                description = "{py(member_father|father)=" + str(member[5]) + "|" + str(father[3]) + "}"
                
                if member[1]!=None:
                    sql = ("UPDATE {0}.members_v2 SET father_id ="+str(father[0]) +" WHERE name_id= '"+str(member[1])+"'").format(schema)
                elif member[2]!=None:
                    sql = ("UPDATE {0}.members_v2 SET father_id ="+str(father[0]) +" WHERE name_ch= '"+str(member[2])+"'").format(schema)
                elif member[3]!=None:
                    sql = ("UPDATE {0}.members_v2 SET father_id ="+str(father[0]) +" WHERE name_py= '"+str(member[3])+"'").format(schema)
                    
                input_log(cursor=cursor, schema = schema,ref = "FATHER",logQuery = sql, desc = description)
                break
    return True
    
def input_log(cursor,schema,ref,logQuery,desc):
    execute_query(
        cursor = cursor,
        query = "INSERT INTO {0}.member_update_log (ref_to,query_update,description,status) VALUES (%s,%s,%s,0);".format(schema),
        inputs = [ref,logQuery,desc],
        get_result=False
    )