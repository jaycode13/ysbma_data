def create_schema(schema):
    return """
    CREATE SCHEMA IF NOT EXISTS {0}
    """.format(schema)
    
def create_tables(schema):
    return """
    DROP TABLE IF EXISTS {0}.member_update_log CASCADE;
    DROP TABLE IF EXISTS {0}.members_v1 CASCADE;
    DROP TABLE IF EXISTS {0}.phone_numbers CASCADE;
    DROP TABLE IF EXISTS {0}.members_v2 CASCADE;
    DROP TABLE IF EXISTS {0}.member_duplicate CASCADE;
    DROP TABLE IF EXISTS {0}.member_ambiguos CASCADE;
    DROP TABLE IF EXISTS {0}.staff CASCADE;
    DROP TABLE IF EXISTS {0}.families CASCADE;
    
    CREATE TABLE {0}.staff (
        id INT GENERATED BY DEFAULT AS IDENTITY,
        name_id         VARCHAR,
        name_ch         VARCHAR,
        name_py         VARCHAR,
        role            VARCHAR,
        PRIMARY KEY (id)
    );
    
    CREATE TABLE {0}.member_ambiguos (
        id INT GENERATED BY DEFAULT AS IDENTITY,
        member_id       INT,
        name_id         VARCHAR,
        name_ch         VARCHAR,
        name_py         VARCHAR,
        gender          VARCHAR,
        birthdate       DATE,
        address         VARCHAR,
        city            VARCHAR,
        desc_input      VARCHAR,
        inputter_id     INT,
        collector_id    INT,
        PRIMARY KEY (id)
    );
    
    CREATE TABLE {0}.member_duplicate (
        id INT GENERATED BY DEFAULT AS IDENTITY,
        member_id       INT,
        name_id         VARCHAR,
        name_ch         VARCHAR,
        name_py         VARCHAR,
        gender          VARCHAR,
        birthdate       DATE,
        address         VARCHAR,
        city            VARCHAR,
        desc_input      VARCHAR,
        PRIMARY KEY (id)
    );
    
    CREATE TABLE {0}.member_update_log (
        id INT GENERATED BY DEFAULT AS IDENTITY,
        ref_to       VARCHAR,
        query_update TEXT,
        description  TEXT,
        status       INT,
        PRIMARY KEY (id)
    );
        
    CREATE TABLE {0}.members_v1 (
        id INT GENERATED BY DEFAULT AS IDENTITY,
        date            DATE,
        name_id         VARCHAR,
        name_ch         VARCHAR,
        name_py         VARCHAR,
        gender          VARCHAR,
        birth           DATE,
        phone           VARCHAR,
        address         VARCHAR,
        city            VARCHAR,
        email           VARCHAR,
        father_id_name  VARCHAR,
        father_ch_name  VARCHAR,
        father_birth    DATE,
        mother_id_name  VARCHAR,
        mother_ch_name  VARCHAR,
        mother_birth    DATE,
        couple_id_name  VARCHAR,
        couple_ch_name  VARCHAR,
        couple_py_name  VARCHAR,
        couple_birth    DATE,
        couple_phone    VARCHAR,
        couple_address  VARCHAR,
        couple_city     VARCHAR,
        from_xian_you   BOOLEAN,
        
        couple_is_xian_you BOOLEAN,
        number_of_child INT,
        
        inputter_id     INT,
        collector_id    INT,
        page_index      VARCHAR,
        is_deceased     BOOL DEFAULT 'f',
        couple_deceased BOOL DEFAULT 'f',
        PRIMARY KEY (id)
    );

    CREATE TABLE {0}.members_v2 (
        id INT GENERATED BY DEFAULT AS IDENTITY,
        date            TIMESTAMPTZ,
        name_id         VARCHAR,
        name_ch         VARCHAR,
        name_py         VARCHAR,
        gender          VARCHAR,
        birthdate       DATE,
        address         VARCHAR,
        city            VARCHAR,
        email           VARCHAR,
        father_id       INT REFERENCES {0}.members_v2(id),
        mother_id       INT REFERENCES {0}.members_v2(id),
        spouse_id       INT REFERENCES {0}.members_v2(id),
        num_children    INT,
        inputter_id     INT REFERENCES {0}.staff(id),
        collector_id    INT REFERENCES {0}.staff(id),
        from_xian_you   BOOL DEFAULT 't',
        is_deceased     BOOL DEFAULT 'f',
        is_verified     BOOL DEFAULT 'f',
        page_index      VARCHAR,
        created_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
        updated_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
        collected_at    TIMESTAMPTZ,
        inputted_at      TIMESTAMPTZ,
        updated_by      VARCHAR,
        num_siblings    INT,
        num_grandchildren  INT,
        verified_by     VARCHAR,
        sibling_order   INT
        PRIMARY KEY (id),
        UNIQUE (name_id, name_ch, name_py, birthdate)
    );
    
    CREATE TABLE {0}.phone_numbers (
        id INT GENERATED BY DEFAULT AS IDENTITY,
        member_id       INT REFERENCES {0}.members_v2(id),
        value           VARCHAR,
        created_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
        updated_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
        PRIMARY KEY (id),
        UNIQUE (value, member_id)
    );
    
    CREATE TABLE {0}.families(
        member_id     integer,
        label         integer,
        debuger       varchar(50),
        UNIQUE (member_id, label)
    );

    CREATE OR REPLACE FUNCTION {0}.func_family_grouping() RETURNS TRIGGER AS $families$
    DECLARE
        father_counter NUMERIC;
        mother_counter NUMERIC;
        spouse_counter NUMERIC;
        relation INTEGER;
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            IF (NEW.father_id IS NOT NULL) THEN
                SELECT INTO father_counter COUNT(*) FROM {0}.families f WHERE f.member_id = NEW.father_id;
                IF (father_counter>=1) THEN
                    SELECT INTO relation f.label FROM {0}.families f WHERE f.member_id = NEW.father_id;
                END IF;
                INSERT INTO {0}.families VALUES (NEW.id, relation, 'father');
            ELSIF (NEW.mother_id IS NOT NULL) THEN
                SELECT INTO mother_counter COUNT(*) FROM {0}.families f WHERE f.member_id = NEW.mother_id;
                IF (mother_counter>=1) THEN
                    SELECT INTO relation f.label FROM {0}.families f WHERE f.member_id = NEW.mother_id;
                END IF;
                INSERT INTO {0}.families VALUES (NEW.id, relation, 'mother');
            ELSIF (NEW.spouse_id IS NOT NULL) THEN
                SELECT INTO spouse_counter COUNT(*) FROM {0}.families f WHERE f.member_id = NEW.spouse_id;
                IF (spouse_counter>=1) THEN
                    SELECT INTO relation f.label FROM {0}.families f WHERE f.member_id = NEW.spouse_id;
                END IF;
                INSERT INTO {0}.families VALUES (NEW.id, relation, 'spouse');
            ELSE
                INSERT INTO {0}.families VALUES (NEW.id, NEW.id, 'new');
            END IF;
            RETURN NEW;
        ELSIF (TG_OP = 'UPDATE') THEN
            IF (NEW.father_id IS NOT NULL) THEN
                SELECT INTO relation f.label FROM {0}.families f WHERE f.member_id = NEW.father_id;
                UPDATE {0}.families SET label = relation, debuger = CONCAT('updFather ',NEW.id) WHERE member_id = NEW.id;
                UPDATE {0}.families SET label = relation, debuger = CONCAT('updFatherFam ',OLD.id) WHERE label = OLD.id;
            ELSIF (NEW.mother_id IS NOT NULL) THEN
                SELECT INTO relation f.label FROM {0}.families f WHERE f.member_id = NEW.mother_id;
                UPDATE {0}.families SET label = relation, debuger = CONCAT('updMother ',NEW.id) WHERE member_id = NEW.id;
                UPDATE {0}.families SET label = relation, debuger = CONCAT('updMotherFam ',OLD.id) WHERE label = OLD.id;
            ELSIF (NEW.spouse_id IS NOT NULL) THEN
                SELECT INTO relation f.label FROM {0}.families f WHERE f.member_id = NEW.spouse_id;
                UPDATE {0}.families SET label = relation, debuger = CONCAT('updSpouse ',NEW.id) WHERE member_id = NEW.id;
                UPDATE {0}.families SET label = relation, debuger = CONCAT('updSpouseFam ',OLD.id) WHERE label = OLD.id;
            END IF;
            RETURN NEW;
        ELSE
            DELETE FROM {0}.families WHERE member_id = OLD.id;
            RETURN OLD;
        END IF;
    END;
    $families$ LANGUAGE plpgsql;

    DROP TRIGGER IF EXISTS family_grouping ON {0}.members_v2 CASCADE;
    
    CREATE TRIGGER family_grouping
    AFTER INSERT OR UPDATE OR DELETE ON {0}.members_v2
        FOR EACH ROW EXECUTE PROCEDURE {0}.func_family_grouping();
    """.format(schema)