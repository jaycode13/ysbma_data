"""
Layers:
1. psycopg2
2. helpers
"""
import json
import os
import traceback
import io
from db_helper import *

def lambda_handler(event, context):
    try:
        cnx = make_connection(event['DBConnection']['endpoint'],
                              event['DBConnection']['port'],
                              event['DBConnection']['dbuser'],
                              event['DBConnection']['dbpassword'],
                              event['DBConnection']['database'])
        cursor=cnx.cursor()
        logger.info('Connected!')
    except:
        return log_err("ERROR: Cannot connect to database from handler.\n{}".format(traceback.format_exc()))
        
    count_member_sql = "select count(id) from {0}.members_v2".format(event["env"])
    cursor.execute(count_member_sql)
    data_count_member = cursor.fetchone()[0]
    
    filling_with_mother_address(
        schema = event["env"],
        cursor = cursor,
        data_count_member = data_count_member
    )
    
    filling_with_father_address(
        schema = event["env"],
        cursor = cursor,
        data_count_member = data_count_member
    )
    
    filling_with_spouse_address(
        schema = event["env"],
        cursor = cursor,
        data_count_member = data_count_member
    )
   
    return True
    
def input_log(cursor,schema,ref,logQuery,desc):
    execute_query(
        cursor = cursor,
        query = "INSERT INTO {0}.member_update_log (ref_to,query_update,description,status) VALUES (%s,%s,%s,1);".format(schema),
        inputs = [ref,logQuery,desc],
        get_result=False
    )
    
def filling_with_mother_address(schema,cursor,data_count_member):
    strQuery = """SELECT
                member.id, mother.address, CONCAT(member.address,'|',mother.address)
            FROM {0}.members_v2 member
            INNER JOIN {0}.members_v2 mother ON member.mother_id=mother.id
            WHERE 
                (member.address IS NULL) AND 
                (mother.address IS NOT NULL)""".format(schema)
                
    members = execute_query(cursor, strQuery, get_result=True)
    for member in members:
        query_update = "UPDATE members_v2 SET address = %s WHERE id = %s"
        execute_query(cursor, query_update, [member[1],member[0]],False)
        description = {
            "id" : member[0],
            "address" : member[1]
        }
        input_log(cursor=cursor,schema=schema,ref="MOTHER_ADDRESS",logQuery=query_update,desc=json.dumps(description))

def filling_with_father_address(schema,cursor,data_count_member):
    strQuery = """SELECT
                member.id, father.address, CONCAT(member.address,'|',father.address)
            FROM {0}.members_v2 member
            INNER JOIN {0}.members_v2 father ON member.father_id=father.id
            WHERE 
                (member.address IS NULL) AND 
                (father.address IS NOT NULL)""".format(schema)
                
    members = execute_query(cursor, strQuery, get_result=True)
    for member in members:
        query_update = "UPDATE members_v2 SET address = %s WHERE id = %s"
        execute_query(cursor, query_update, [member[1],member[0]],False)
        description = {
            "id" : member[0],
            "address" : member[1]
        }
        input_log(cursor=cursor,schema=schema,ref="FATHER_ADDRESS",logQuery=query_update,desc=json.dumps(description))
        
def filling_with_spouse_address(schema,cursor,data_count_member):
    strQuery = """SELECT
                member.id, spouse.address, CONCAT(member.address,'|',spouse.address)
            FROM {0}.members_v2 member
            INNER JOIN {0}.members_v2 spouse ON member.spouse_id=spouse.id
            WHERE 
                (member.address IS NULL) AND 
                (spouse.address IS NOT NULL)""".format(schema)
                
    members = execute_query(cursor, strQuery, get_result=True)
    for member in members:
        query_update = "UPDATE members_v2 SET address = %s WHERE id = %s"
        execute_query(cursor, query_update, [member[1],member[0]],False)
        description = {
            "id" : member[0],
            "address" : member[1]
        }
        input_log(cursor=cursor,schema=schema,ref="SPOUSE_ADDRESS",logQuery=query_update,desc=json.dumps(description))