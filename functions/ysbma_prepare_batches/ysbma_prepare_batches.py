"""
Layers:
1. googleapi
"""
import traceback
import io
import sys
import json
import boto3
import sys
import os

def lambda_handler(event, context):
    google_base_url = "https://sheets.googleapis.com/v4/spreadsheets/{}/values:batchGet?ranges=A{}:AZ{}&key={}"
    API_URL = []
    index = 0
    incremental = 500
    while index < event["Rows"]:
        if(index==0):
            API_URL.append(google_base_url.format(event["GoogleApi"]["SpreadsheetId"], 2, incremental,event["GoogleApi"]["ApiKey"]))
            index=incremental
        else:
            if(index+incremental < event["Rows"]):
                API_URL.append(google_base_url.format(event["GoogleApi"]["SpreadsheetId"], index+1, index+incremental,event["GoogleApi"]["ApiKey"]))
            else:
                API_URL.append(google_base_url.format(event["GoogleApi"]["SpreadsheetId"], index+1, event["Rows"],event["GoogleApi"]["ApiKey"]))
            index = index + incremental

    API_URL.append("DONE")
    
    return API_URL
