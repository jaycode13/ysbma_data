"""
Layers:
1. psycopg2
2. helpers
"""

import json
import os
import traceback
import io
from db_helper import *
import queries as q

def lambda_handler(event, context):
    try:

        cnx = make_connection(event['DBConnection']['endpoint'],
                              event['DBConnection']['port'],
                              event['DBConnection']['dbuser'],
                              event['DBConnection']['dbpassword'],
                              event['DBConnection']['database'])
        cursor=cnx.cursor()
        logger.info('Connected!')
    except:
        return log_err("ERROR: Cannot connect to database from handler.\n{}".format(traceback.format_exc()))
        
    execute_query(cursor, q.create_staff_table(schema = event['env']), get_result=False)
    execute_query(cursor, q.input_staff_table(schema = event['env']), get_result=False)
    
    logger.info('Tables Staff created!')
    
    return True
    
    # TODO implement
    # return {
    #     'statusCode': 200,
    #     'body': json.dumps('Hello from Lambda!')
    # }
