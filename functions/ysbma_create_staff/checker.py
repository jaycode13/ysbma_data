import json
import os
import traceback
import io
from db_helper import *

def lambda_handler(event, context):
    try:

        cnx = make_connection(event['DBConnection']['endpoint'],
                              event['DBConnection']['port'],
                              event['DBConnection']['dbuser'],
                              event['DBConnection']['dbpassword'],
                              event['DBConnection']['database'])
        cursor=cnx.cursor()
        logger.info('Connected!')
        
        postgreSQL_select_Query = "select * from information_schema.tables where table_schema='demo'"
        cursor.execute(postgreSQL_select_Query)
        print("Selecting rows from staff table using cursor.fetchall")
        mobile_records = cursor.fetchall() 
       
        print("Print each row and it's columns values")
        for row in mobile_records:
            print(row[0] + " - " + row[1] + " - " + row[2] )
            
        #-------------------------
        postgreSQL_select_Query = "INSERT INTO demo.staff(name_id,name_ch,name_py) VALUES ('null','傅慶國','Fu Qing Guo')"
        cursor.execute(postgreSQL_select_Query)
        #-------------------------
        postgreSQL_select_Query = "select * from demo.staff"
        cursor.execute(postgreSQL_select_Query)
        print("Selecting rows from staff table using cursor.fetchall")
        mobile_records = cursor.fetchall() 
       
        print("Print each row and it's columns values")
        for row in mobile_records:
            print(row)
    except:
        return log_err("ERROR: Cannot connect to database from handler.\n{}".format(traceback.format_exc()))
    
    return {
        "DBConnection": {
            "endpoint":     event['DBConnection']['endpoint'],
            "port":         event['DBConnection']['port'],
            "dbuser":       event['DBConnection']['dbuser'],
            "dbpassword":   event['DBConnection']['dbpassword'],
            "database":     event['DBConnection']['database']   
        },
        'Rows' :   event['Rows'],
        "Access":  event['Access'],
        "env" :    event['env'],
        'GoogleApi':{
            'Information'    : event['GoogleApi']['Information'],
            'ApiKey'         : event['GoogleApi']['ApiKey'],
            'SpreadsheetId'  : event['GoogleApi']['SpreadsheetId']
        },
        'CreateTableSuccess': True
    }
